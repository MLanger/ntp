/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	target_specific_mutex.hpp
 * @details different mutex-implementations depending on the target-system
 * @date 	31.07.2017
 * @author 	Silvan König
 * @version	0.1
 */

#ifndef NTP_UTILITY_TARGET_SPECIFIC_MUTEX_HPP_
#define NTP_UTILITY_TARGET_SPECIFIC_MUTEX_HPP_

#ifdef __MINGW32__
// Is defined if MinGW32 or MinGW32-w64 is used.
#include "../../externalLibrary/mingw.mutex.h"
#include <mutex>

#elif __GNUC__
// Is defined if GCC is used. This usually means the Software should run on a Linux machine.
// In this case, we can use the stdlib header.
#include <mutex>

#endif

#endif /* NTP_UTILITY_TARGET_SPECIFIC_MUTEX_HPP_ */
