/**
 * @file 	PhaseFrequencyLockLoop.cpp
 * @details This file actually only initializes the static variables of class
 * 			PhaseFrequencyLockLoop.
 * @date 	13.03.2018
 * @author 	silvan
 * @version	1
 */

#include "KernelInterface.hpp"

#ifdef NTP_WINDOWS

#include "PhaseFrequencyLockLoop.hpp"

namespace ntp
{

DWORD PhaseFrequencyLockLoop::TIMEADJUSTMENT_MAX = 17000; ///< maximum value for TimeAdjustment
DWORD PhaseFrequencyLockLoop::TIMEADJUSTMENT_MIN = 14000; ///< minimum value for TimeAdjustment

} // namespace ntp

#endif
