/*
* ----------------------------------------------------------------------------
* Copyright 2018 Ostfalia University of Applied Sciences, Germany
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ----------------------------------------------------------------------------
*/

/**
 * @file 	PhaseFrequencyLockLoop.hpp
 * @details This Header declares the class PhaseFrequencyLockLoop.
 * @date 	05.03.2018
 * @author 	Silvan König
 * @version	1.0
 */

#ifndef NTP_UTILITY_PHASEFREQUENCYLOCKLOOP_HPP_
#define NTP_UTILITY_PHASEFREQUENCYLOCKLOOP_HPP_

#include "../Log/Logger.hpp"
#include "windows.h"
#include <chrono>
#include <cmath>

/*
 * Clock discipline parameters and constants
 * (as specified in RFC5905)
 */
#define PANICT 1000000000LL /* panic threshold (us) */
#define FLL 4               /* FLL loop gain */

/// averaging constant as defined in RFC5905
#define AVG 8

// in RFC5905 this is a double value (=500e-6).
// unfortunately, shifting a double value is not allowed (operator<<)
#define MAXFREQ 5000LL ///< frequency tolerance (500 ppm)

/// found by experiments
#define SHIFT_PLL 4

#define USEC_PER_SEC 1000000LL
#define NSEC_PER_USEC 1000LL
#define MAXPHASE (500LL * NSEC_PER_USEC * USEC_PER_SEC) ///< 500 ms, taken from Linux source code

/**
 * @brief	This macro will return VALUE if it is in the Range MINVAL <= VALUE => MAXVAL.
 * @details	If VALUE is higher than MAXVAL, MAXVAL will be returned.
 * 			If VALUE is lower than MINVAL, MINVAL will be returned.
 */
#define clamp(VALUE, MINVAL, MAXVAL) (VALUE > MINVAL ? (VALUE < MAXVAL ? VALUE : MAXVAL) : MINVAL)

// FLL parameters (copied from Linux source)
/**
 * @name FLL Limits
 * @{
 */
#define MINSEC 256  ///< min interval between updates (s)
#define MAXSEC 2048 ///< max interval between updates (s)
/**
 *	@}
 */

/**
 * @brief	used to accomplish higher accuracy during division operations.
 * @details	If using double as offsettype, this is not necessary anymore.
 */
#define NTP_SCALE_SHIFT 32

namespace ntp
{

/**
 * @brief	Implementation of the PLL and FLL algorithm as described in RFC5905
 * @details	This class implements the PLL and FLL algorithm similiar to RFC5905.
 * 			It was also inspired by the implementation in the Linux-Kernel.
 *
 * 			This class is designed as a blackbox which is doing complex calculations.
 * 			You feed it with the current offset using the adjustTime() method and get
 * 			the results using the getters.
 *
 * 			It is intended to be used with Windows only. When porting the application
 * 			to another platform, this implementation is likely to be useless.
 *
 *			It is important to notice, that Windows calculates the clock update interval
 *			on startup as noticed on https://msdn.microsoft.com/de-de/library/windows/desktop/ms724943.aspx .
 *			Since it is not clear how this is done and which possible values there are,
 *			the TIMEADJUSTMENT_MAX and TIMEADJUSTMENT_MIN values of this class are not constants
 *			but calculated on instantiation of this class.
 */
class PhaseFrequencyLockLoop
{

public:
	typedef std::chrono::high_resolution_clock::time_point timestamptype;
	typedef signed long long offsettype; // must be signed, may become negative

	static double constexpr PRECISION = 100.0e-9; ///< system precision (is 100 ns)

	/// maximum time constant
	static unsigned int constexpr MAXTC = 10;
	/// minimum time constant
	static unsigned int constexpr MINTC = 0;

private:
	timestamptype lastTimestamp;
	offsettype offset;
	offsettype lastoffset;      // used to calculate jitter
	offsettype frequencyOffset; // scaled by NTP_SCALE_SHIFT
	int timeConstant;
	DWORD TimeIncrementInterval; /// Windows clock update interval
	DWORD TimeAdjustment;
	double jitter; //< contains the result of the latest jitter calculation

	static DWORD TIMEADJUSTMENT_MAX; ///< maximum value for TimeAdjustment
	static DWORD TIMEADJUSTMENT_MIN; ///< minimum value for TimeAdjustment

	static constexpr unsigned int FREQ_RESOLUTION = 100; ///< Windows frequency correction resolution is 100ns

public:
	/**
	 * @details	Reset internal values and get some parameters from Windows.
	 * 			Also calculates TIMEADJUSTMENT_MAX to be 5 % above TimeIncrementInterval
	 * 			and TIMEADJUSTMENT_MIN to be 5 % below TimeIncrementInterval.
	 */
	inline PhaseFrequencyLockLoop()
	{
		reset();

		BOOL TimeAdjustmentDisabled;

		// The TimeAdjustment is calculated once, when the system is started.
		BOOL result = GetSystemTimeAdjustment(&TimeAdjustment,
		                                      &TimeIncrementInterval,
		                                      &TimeAdjustmentDisabled);
		this->TimeAdjustment = this->TimeIncrementInterval;

		PhaseFrequencyLockLoop::TIMEADJUSTMENT_MAX = this->TimeIncrementInterval * 1.05;
		PhaseFrequencyLockLoop::TIMEADJUSTMENT_MIN = this->TimeIncrementInterval * 0.95;
	}

	/**
	 * @brief	resets all internal values and restart timer (lastTimestamp)
	 */
	inline void reset()
	{
		using namespace std::chrono;

		this->lastTimestamp = high_resolution_clock::now(); // zero will lead to problems
		                                                    // when adjusting the clock for the first time.
		this->offset = MAXPHASE;
		this->lastoffset = 0;
		this->frequencyOffset = 0;
		this->timeConstant = 2;
		this->jitter = PRECISION;
	}

	/**
	 * @brief	sets the time constant just like ntp_adjtime() does under Linux
	 * @details	The given time constant must be withing the Range from MINTC to MAXTC.
	 */
	inline void setTimeConstant(unsigned int timeConstant)
	{
		this->timeConstant = clamp(timeConstant, MINTC, MAXTC);
	}

	/**
	 * @brief	calculates the parameters for correcting the clock (offset/frequency offset)
	 * @details	This method does all the magic. It takes the offset and calculates the offset to
	 * 			be used to correct the clock as well as the frequency offset to correct the frequency
	 * 			drift.
	 *
	 * @param 	offset [in] The offset in nanoseconds as computed by the Peer- and System-process.
	 */
	inline void adjustClock(offsettype offset)
	{
		using namespace std::chrono;

		// if the offset is above the panic threshold we can stop here
		if (abs(offset) > PANICT)
			return;

		timestamptype now = high_resolution_clock::now();

		// get interval since last update in seconds
		nanoseconds lastUpdateInterval_duration = now - this->lastTimestamp;
		seconds lUId_secs = duration_cast<seconds>(lastUpdateInterval_duration);
		seconds::rep lastUpdateInterval = lUId_secs.count();

		this->lastTimestamp = now; // update last updatetime

		offset = clamp(offset, -MAXPHASE, MAXPHASE);

		// calculate jitter
		double a = this->jitter * this->jitter;
		double lastoffsetdifference = fmax(abs(offset - this->lastoffset) * 1.0e-9, PRECISION); // offset is nanoseconds, precision is seconds
		double b = lastoffsetdifference * lastoffsetdifference;
		this->jitter = sqrt(a + (a - b) / AVG);

		// calculate frequency correction due to FLL (freq_adj)
		offsettype freq_adj = 0;
		if (lastUpdateInterval > MINSEC && lastUpdateInterval < MAXSEC)
		{
			/*
				The frequency difference is how much the clock ran too 
				fast/slow since the last update. The time from last update
				till now is lastUpdateInterval. The clock drift is "offset".
				So, if the clock is ticking in the right speed, offset is zero
				and the frequency adjustment becomes zero as well.
				FLL slows the adjustment down a little bit.
			*/
			freq_adj = (offset << NTP_SCALE_SHIFT) / FLL / (lastUpdateInterval << NTP_SCALE_SHIFT);
			DEBUG(std::string("PLL frequency due to FLL: ") + std::to_string(freq_adj));
		}

		// taken from Linux source. Don't know.
		if (lastUpdateInterval > (1 << (SHIFT_PLL + 1 + this->timeConstant)))
		{
			lastUpdateInterval = 1 << (SHIFT_PLL + 1 + this->timeConstant);
		}

		// calculate new frequency offset, divide by denominator and frequency resolution
		offsettype scaledTmpValue = ((offset * lastUpdateInterval) << NTP_SCALE_SHIFT);
		freq_adj += scaledTmpValue / (FREQ_RESOLUTION << (2 * (SHIFT_PLL + 2 + this->timeConstant)));
		DEBUG(std::string("PLL calculated freq adjustment: ") + std::to_string(freq_adj));

		// add old frequency offset to new offset
		freq_adj += this->frequencyOffset;

		// freq_adj was scaled so we must scale MAXFREQ as well.
		this->frequencyOffset = clamp(freq_adj, -(MAXFREQ << NTP_SCALE_SHIFT), (MAXFREQ << NTP_SCALE_SHIFT));

		// (1<<6) where 6 (seconds) is likely the poll interval
		this->offset = (offset << NTP_SCALE_SHIFT) / (1 << 6);

		this->lastoffset = offset;
	}

	/**
	 * @brief	returns the raw frequency correction in nanoseconds
	 */
	inline offsettype getRawFrequencyCorrection() const
	{
		return (this->frequencyOffset / FREQ_RESOLUTION >> NTP_SCALE_SHIFT);
	}

	/**
	 * @brief	returns the calculated frequency in 100 nanoseconds resolution
	 * @details	This methods output is intended to be used as input for the
	 * 			GetSystemTimeAdjustment() function provided by Windows.
	 *
	 * 			The frequency will be clamped to be within TIMEADJUSTMENT_MAX and TIMEADJUSTMENT_MIN.
	 */
	inline offsettype getFrequency() const
	{
		DEBUG(std::string("PLL realFrequencyCorrection: ") + std::to_string(getRawFrequencyCorrection()));
		return (clamp(getRawFrequencyCorrection() + this->TimeAdjustment, TIMEADJUSTMENT_MIN, TIMEADJUSTMENT_MAX));
	}

	/**
	 * @brief	returns the offset in nanoseconds as it was calculated by the algorithm.
	 */
	inline offsettype getOffset() const
	{
		return (this->offset >> NTP_SCALE_SHIFT);
	}

	/**
	 * @brief	returns the clock jitter as specified in RFC5905
	 */
	inline double getJitter() const
	{
		return this->jitter;
	}

}; // end of class PhaseFrequencyLockLoop

} // end of namespace ntp

#endif
