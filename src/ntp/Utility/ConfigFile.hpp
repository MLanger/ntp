/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

#ifndef NTP_UTILITY_CONFIGFILE_HPP_
#define NTP_UTILITY_CONFIGFILE_HPP_

/**
 * @file        ConfigFile.hpp
 * @brief       Small parser for reading and writing configuration files (.cfg / .ini)
 *
 * @version   	1.0.0
 * @date      	2016
 * @author
*/

// =========================================================================================================================================
// ==========================================================  required includes  ==========================================================
// =========================================================================================================================================

// standard header
#include "../Log/Logger.hpp"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

// =========================================================================================================================================
// ==========================================================  defines and macros  =========================================================
// =========================================================================================================================================

// =========================================================================================================================================
// ============================================================  declare class  ============================================================
// =========================================================================================================================================

namespace ntp
{

typedef std::map<std::string, std::string> configConfigMap;
typedef std::map<std::string, configConfigMap> configSectionMap;

/**
 * @details This class handles configuration files according to the rules of an initialization file (.ini).
 * 			Each object can manage one configuration file. The access to the configuration file is done with read and write rights.
 * 			Doesn't exist the configuration file, it will be created automatically.
 *
 * 			The following properties are applied in the processing of configuration files:
 *
 * 			- Each section must be unique. A second section with the same name will be ignored.
 * 			- Each key must be unique per section. A second key per section with the same name will be ignored.
 * 			- Comments starts with ';'
 * 			- Blank lines are allowed.
 * 			- Tabulators and Spaces between the key and '=' are allowed
 * 			- Tabulators and Spaces between '=' and the value are allowed
 * 			- Upper/lower case is not distinguished (i.e. case insensitive for section and key)
 * 			- Allowed characters for section and key are: a-z, A-Z, 0-9, underscore (_), dot (.) and space ( ).
 * 			- For boolean values: '1', 'true' and 'yes' are interpreted as 'true'
 * 			- For boolean values: '0', 'false' and 'no' are interpreted as 'false'
 * 			- For string values: All characters are allowed (inclusive umlauts and special characters). The value is case sensitive.
 *
 *			Example for a configuration file:
~~~~~~~
;comment
[section]
;comment
key=value
~~~~~~~
*/
class ConfigFile
{
private:
	static ConfigFile instance;

	ConfigFile();
	ConfigFile(std::string const& filename);
	~ConfigFile();

	bool isSectionNameValid(std::string const& section) const;
	bool isKeyNameValid(std::string const& key) const;

	bool readConfigKey(std::string& dst, std::string section, std::string key) const;
	bool writeConfigKey(std::string const& value, std::string section, std::string key) const;

	// this is mutable, because I was to lazy to change this const-shit.
	mutable configSectionMap sectionMapping;
	std::string m_configFileName; ///< full path and file name of the configuration file

public:
	///@name general functions
	///@{
	inline static ConfigFile& getInstance();
	///@}

	///@name specific functions
	///@{
	bool existConfigFile();
	bool isConfigFileValid();

	std::string getConfigFile() const;
	void setConfigFile(std::string const& filename);

	bool readBool(bool& dst, std::string const& section, std::string const& key, bool defaultValue = false) const;
	bool writeBool(bool value, std::string const& section, std::string const& key) const;

	bool readInt(int& dst, std::string const& section, std::string const& key, int defaultValue = 0) const;
	bool writeInt(int value, std::string const& section, std::string const& key) const;

	bool readLong(long& dst, std::string const& section, std::string const& key, long defaultValue = 0) const;
	bool writeLong(long value, std::string const& section, std::string const& key) const;

	bool readULong(unsigned long& dst, std::string const& section, std::string const& key, unsigned long defaultValue = 0) const;
	bool writeULong(unsigned long value, std::string const& section, std::string const& key) const;

	bool readLongLong(long long& dst, std::string const& section, std::string const& key, long long defaultValue = 0) const;
	bool writeLongLong(long long value, std::string const& section, std::string const& key) const;

	bool readULongLong(unsigned long long& dst, std::string const& section, std::string const& key, unsigned long long defaultValue = 0) const;
	bool writeULongLong(unsigned long long value, std::string const& section, std::string const& key) const;

	bool readFloat(float& dst, std::string const& section, std::string const& key, float defaultValue = 0.0) const;
	bool writeFloat(float value, std::string const& section, std::string const& key) const;

	bool readDouble(double& dst, std::string const& section, std::string const& key, double defaultValue = 0.0) const;
	bool writeDouble(double value, std::string const& section, std::string const& key) const;

	bool readLongDouble(long double& dst, std::string const& section, std::string const& key, long double defaultValue = 0.0) const;
	bool writeLongDouble(long double value, std::string const& section, std::string const& key) const;

	bool readString(std::string& dst, std::string const& section, std::string const& key, std::string const& defaultValue = "") const;
	bool writeString(std::string const& value, std::string const& section, std::string const& key) const;
	///@}

	void parseFile() const;
};

inline ConfigFile& ConfigFile::getInstance() { return ConfigFile::instance; }

} //end of namespace ntp

#endif // NTP_UTILITY_CONFIGFILE_HPP_
// END OF FILE
