/*
* ----------------------------------------------------------------------------
* Copyright 2018 Ostfalia University of Applied Sciences, Germany
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ----------------------------------------------------------------------------
*/

/**
 * @file BoostUDPTimeoutTransmitter.cpp
 *
 * @date 	06.03.2018
 * @author 	Silvan König
 * @version 1.0
 *
 */

#ifndef NTP_UTILITY_BOOSTUDPTIMEOUTTRANSMITTER_HPP_
#define NTP_UTILITY_BOOSTUDPTIMEOUTTRANSMITTER_HPP_

#include <boost/asio.hpp>

#include "../Datatypes/Exceptions/TimeoutException.hpp"
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>

namespace ntp
{

//----------------------------------------------------------------------
/**
 * 	The following class is used internally. It is a necessary workaround
 * 	to create a receive-method which blocks until either a given time was expired
 * 	or a new packet has been received.
 *
 * 	The code was taken from:
 * 	http://www.boost.org/doc/libs/1_65_1/doc/html/boost_asio/example/cpp03/timeouts/blocking_udp_client.cpp
 *
 */

//
// blocking_udp_client.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
//
// This class manages socket timeouts by applying the concept of a deadline.
// Each asynchronous operation is given a deadline by which it must complete.
// Deadlines are enforced by an "actor" that persists for the lifetime of the
// client object:
//
//  +----------------+
//  |                |
//  | check_deadline |<---+
//  |                |    |
//  +----------------+    | async_wait()
//              |         |
//              +---------+
//
// If the actor determines that the deadline has expired, any outstanding
// socket operations are cancelled. The socket operations themselves are
// implemented as transient actors:
//
//   +---------------+
//   |               |
//   |    receive    |
//   |               |
//   +---------------+
//           |
//  async_-  |    +----------------+
// receive() |    |                |
//           +--->| handle_receive |
//                |                |
//                +----------------+
//
// The client object runs the io_service to block thread execution until the
// actor completes.
//

using boost::asio::deadline_timer;
using boost::asio::ip::udp;

class BoostUDPTimeoutTransmitter
{

public:
	BoostUDPTimeoutTransmitter(boost::asio::ip::udp::socket& sock)
	    : io_service_(sock.get_io_service()),
	      socket_(sock),
	      deadline_(io_service_)
	{
		// No deadline is required until the first socket operation is started. We
		// set the deadline to positive infinity so that the actor takes no action
		// until a specific deadline is set.
		deadline_.expires_at(boost::posix_time::pos_infin);

		lastEvent = LastEvent::NONE;
		// Start the persistent actor that checks for deadline expiry.
		check_deadline();
	}

	inline std::size_t receive(const boost::asio::mutable_buffer& buffer,
	                           boost::posix_time::time_duration timeout, boost::system::error_code& ec,
	                           boost::asio::ip::udp::endpoint& ep)
	{
		// Set a deadline for the asynchronous operation.
		deadline_.expires_from_now(timeout);

		// Set up the variables that receive the result of the asynchronous
		// operation. The error code is set to would_block to signal that the
		// operation is incomplete. Asio guarantees that its asynchronous
		// operations will never fail with would_block, so any other value in
		// ec indicates completion.
		ec = boost::asio::error::would_block;
		std::size_t length = 0;

		// Start the asynchronous operation itself. The handle_receive function
		// used as a callback will update the ec and length variables.
		socket_.async_receive_from(boost::asio::buffer(buffer), ep,
		                           boost::bind(&BoostUDPTimeoutTransmitter::handle_receive, this, _1, _2, &ec, &length));

		// Block until the asynchronous operation has completed.
		do
			io_service_.run_one();
		while (ec == boost::asio::error::would_block);

		if (this->lastEvent == LastEvent::TIMEOUT)
		{
			throw ntp::TimeoutException("Timeout");
		}

		return length;
	}

private:
	void check_deadline()
	{
		// Check whether the deadline has passed. We compare the deadline against
		// the current time since a new asynchronous operation may have moved the
		// deadline before this actor had a chance to run.
		if (deadline_.expires_at() <= deadline_timer::traits_type::now())
		{
			// The deadline has passed. The outstanding asynchronous operation needs
			// to be cancelled so that the blocked receive() function will return.
			//
			// Please note that cancel() has portability issues on some versions of
			// Microsoft Windows, and it may be necessary to use close() instead.
			// Consult the documentation for cancel() for further information.
			socket_.cancel();

			// There is no longer an active deadline. The expiry is set to positive
			// infinity so that the actor takes no action until a new deadline is set.
			deadline_.expires_at(boost::posix_time::pos_infin);

			this->lastEvent = TIMEOUT;
		}

		// Put the actor back to sleep.
		deadline_.async_wait(boost::bind(&BoostUDPTimeoutTransmitter::check_deadline, this));
	}

	void handle_receive(
	    const boost::system::error_code& ec, std::size_t length,
	    boost::system::error_code* out_ec, std::size_t* out_length)
	{
		*out_ec = ec;
		*out_length = length;

		if (this->lastEvent != TIMEOUT)
		{
			this->lastEvent = RECEIVED;
		}
	}

public:
	/*
   * constants of events
   */
	enum LastEvent
	{
		NONE,
		TIMEOUT,
		RECEIVED
	};

private:
	// used to record the last event, which happened
	LastEvent lastEvent;
	boost::asio::io_service& io_service_;
	udp::socket& socket_;
	boost::asio::deadline_timer deadline_;

}; // end of class client

//----------------------------------------------------------------------

} // end of namespace ntp

#endif /* NTP_UTILITY_BOOSTUDPTIMEOUTTRANSMITTER_HPP_ */
