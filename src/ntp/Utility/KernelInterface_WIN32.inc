/*
* ----------------------------------------------------------------------------
* Copyright 2018 Ostfalia University of Applied Sciences, Germany
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ----------------------------------------------------------------------------
*/

/**
 * @file 	KernelInterface_WIN32.cpp
 * @details This file implements the parts of the KernelInterface-Class, that are used in Windows only
 * @date 	20.11.2017
 * @author 	Silvan König
 * @version	1.0
 *
 */

#include <psapi.h>
#include <windows.h>

#include "ScopedSemaphore.hpp"
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>

namespace ntp
{

bool KernelInterface::doProcessesHaveSameName(pid_t processOne, pid_t processTwo)
{

	HANDLE hprocess;
	char name[255];
	TCHAR* nameptr = name;
	DWORD name_length = 255;

	hprocess = OpenProcess(PROCESS_QUERY_INFORMATION, TRUE, processOne);

	DEBUG(std::string("pid1: ") + std::to_string(processOne));
	DEBUG(std::string("pid2: ") + std::to_string(processTwo));

	if (hprocess == 0)
	{
		// Get the stupid windows error message.
		char winerrmsg[255];
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		              NULL,
		              GetLastError(),
		              LANG_USER_DEFAULT,
		              winerrmsg,
		              255,
		              NULL);

		LOG_ERROR(std::string("Windows error message: ") + winerrmsg);

		DEBUG("NULLPOINTER");
	}

	name_length = GetProcessImageFileName(hprocess,
	                                      nameptr,
	                                      name_length);

	std::string processOneName;

	if (name_length > 0)
	{
		processOneName = name;
	}

	DEBUG(std::string("p1Name: ") + processOneName);

	hprocess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, processTwo);

	if (hprocess == 0)
	{
		// Get the stupid windows error message.
		char winerrmsg[255];
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		              NULL,
		              GetLastError(),
		              LANG_USER_DEFAULT,
		              winerrmsg,
		              255,
		              NULL);

		LOG_ERROR(std::string("Windows error message: ") + winerrmsg);

		DEBUG("NULLPOINTER");
	}

	name_length = 255;
	name_length = GetProcessImageFileName(hprocess,
	                                      nameptr,
	                                      name_length);

	std::string processTwoName;

	DEBUG(std::string("p2Name: ") + processTwoName);

	if (name_length > 0)
	{
		processTwoName = name;
	}

	if (processOneName == processTwoName)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool KernelInterface::isOtherProcessRunning()
{

	ScopedSemaphore<boost::interprocess::named_semaphore> _s(this->shmSemaphore);

	bool result = false;

	DWORD myId = GetCurrentProcessId();

	DWORD sharedId = 0;

	switch (ntp::NTP::getMode())
	{
	case ntp::NTP::AppMode::MODE_CLIENT:
		sharedId = *static_cast<DWORD*>(this->sharedClientProcessId.get_address());
		break;
	case ntp::NTP::AppMode::MODE_SERVER:
		sharedId = *static_cast<DWORD*>(this->sharedServerProcessId.get_address());
		break;
	}

	if (sharedId == myId)
	{
		result = false; // We are already part of that semaphore
	}
	else
	{
		if (doProcessesHaveSameName(sharedId, myId))
		{
			// process was started twice
			result = true;
		}
		else
		{
			switch (ntp::NTP::getMode())
			{
			case ntp::NTP::AppMode::MODE_CLIENT:
				*static_cast<DWORD*>(sharedClientProcessId.get_address()) = myId;
				break;
			case ntp::NTP::AppMode::MODE_SERVER:
				*static_cast<DWORD*>(sharedServerProcessId.get_address()) = myId;
				break;
			}

			result = false;
		}
	}

	return result;
}

ntp::LongTimestamp KernelInterface::getTime()
{
	double seconds;

	// version 2
	FILETIME ft;
	GetSystemTimeAsFileTime(&ft);

	ULARGE_INTEGER timeInt;

	timeInt.HighPart = ft.dwHighDateTime;
	timeInt.LowPart = ft.dwLowDateTime;

	seconds = 100.0e-9 * timeInt.QuadPart - UNIX_WINDOWS_TIME_DIFFERENCE_SECONDS + Timestamp::UNIX_NTP_TIME_OFFSET;

	DEBUG(std::string("getTime(): Anzahl Sekunden: ") + std::to_string(seconds));

	ntp::LongTimestamp tmstmp{seconds};

	return tmstmp;
}

void KernelInterface::adjustTime(double offset)
{
	DEBUG("#### adjustTime() called #####");

	// convert from seconds to microseconds
	long long offset_nsec = static_cast<long long>(offset * 1.0e9);
	this->pfll.adjustClock(offset_nsec);

	PhaseFrequencyLockLoop::offsettype pfll_off = this->pfll.getOffset();
	DEBUG(std::string("PLL/FLL offset: ") + std::to_string(pfll_off));
	DEBUG(std::string("new freqAdjust: ") + std::to_string(this->pfll.getFrequency()));

	ULARGE_INTEGER timeOffsetInt;
	timeOffsetInt.QuadPart = 0;
	//	timeOffsetInt.QuadPart = offset / 100.0e-9;

	FILETIME ft;
	GetSystemTimeAsFileTime(&ft);

	ULARGE_INTEGER curTimeInt;
	curTimeInt.HighPart = ft.dwHighDateTime;
	curTimeInt.LowPart = ft.dwLowDateTime;

	curTimeInt.QuadPart += timeOffsetInt.QuadPart;
	curTimeInt.QuadPart += pfll_off;

	ft.dwHighDateTime = curTimeInt.HighPart;
	ft.dwLowDateTime = curTimeInt.LowPart;

	SYSTEMTIME st;
	if (!FileTimeToSystemTime(&ft, &st))
	{
		throw ntp::TimeException("Error adjusting time!");
	}

	if (!SetSystemTime(&st))
	{
		throw ntp::TimeException("Error adjusting time!");
	}

	SetSystemTimeAdjustment(this->pfll.getFrequency(), FALSE); // FALSE means that periodic
	                                                           // time adjustment will be enabled

	DEBUG("#### SYSTEMTIME adjusted #####");
}

void KernelInterface::setFrequencyOffset(double frequencyOffset)
{
	DWORD TimeAdjustment;
	DWORD AdjustmentIntervalTime;
	BOOL TimeAdjustmentDisabled;

	// todo: getting the TimeAdjustment once (constructor) is enough.
	// The TimeAdjustment is calculated once, when the system is started.
	BOOL result = GetSystemTimeAdjustment(&TimeAdjustment,
	                                      &AdjustmentIntervalTime,
	                                      &TimeAdjustmentDisabled);

	TimeAdjustment = static_cast<DWORD>(
	    frequencyOffset * AdjustmentIntervalTime * 1.0e-6);

	DEBUG(std::to_string(frequencyOffset) + "(frequencyOffset)");
	DEBUG(std::to_string(TimeAdjustment) + "(TimeIncrement)");

	result = SetSystemTimeAdjustment(TimeAdjustment, FALSE); // FALSE means that periodic
	                                                         // time adjustment is to be disabled
}

double KernelInterface::getFrequencyOffset()
{
	DWORD TimeAdjustment;
	DWORD TimeIncrementInterval;
	BOOL TimeAdjustmentDisabled;

	double frequencyOffset = 0.0;

	// todo: getting the TimeAdjustment once (constructor) is enough.
	// The TimeAdjustment is calculated once, when the system is started.
	BOOL result = GetSystemTimeAdjustment(&TimeAdjustment,
	                                      &TimeIncrementInterval,
	                                      &TimeAdjustmentDisabled);

	//	 frequencyOffset = TimeAdjustment * 1.0 / TimeIncrementInterval ;
	frequencyOffset = ((signed long)TimeAdjustment - (signed long)TimeIncrementInterval) * 6.41022;

	DEBUG(std::to_string(TimeAdjustment) + "(TimeAdjustment");
	DEBUG(std::to_string(TimeIncrementInterval) + "(TimeIncrementInterval)");
	DEBUG(std::to_string(frequencyOffset) + "(frequencyOffset)");

	return frequencyOffset;
}

void KernelInterface::setTime(double offset)
{
	DEBUG("#### setTime() called #####");
	ULARGE_INTEGER timeOffsetInt;
	timeOffsetInt.QuadPart = offset / 100.0e-9;

	FILETIME ft;
	GetSystemTimeAsFileTime(&ft);

	ULARGE_INTEGER curTimeInt;
	curTimeInt.HighPart = ft.dwHighDateTime;
	curTimeInt.LowPart = ft.dwLowDateTime;

	curTimeInt.QuadPart += timeOffsetInt.QuadPart;

	ft.dwHighDateTime = curTimeInt.HighPart;
	ft.dwLowDateTime = curTimeInt.LowPart;

	SYSTEMTIME st;
	if (!FileTimeToSystemTime(&ft, &st))
	{
		throw ntp::TimeException("Error setting time!");
	}

	if (!SetSystemTime(&st))
	{
		throw ntp::TimeException("Error setting time!");
	}

	DEBUG("#### SYSTEMTIME was set #####");
}

signed char KernelInterface::getSystemPrecision()
{
	return -23;
}

double KernelInterface::getJitter()
{
	return pfll.getJitter();
}

double KernelInterface::getOffset()
{
	return pfll.getOffset() * 1.0e-9;
}

void KernelInterface::insertLeapSecond()
{
	DEBUG("Not possible to insert LeapSecond. Not supported by Windows");
}

void KernelInterface::deleteLeapSecond()
{
	DEBUG("Not possible to delete LeapSecond. Not supported by Windows.");
}

void KernelInterface::resetLeapSecond()
{
	DEBUG("Not possible to reset LeapSecond. Not supported by Windows.");
}

ntp::LeapIndicator KernelInterface::getLeapSecond()
{
	return ntp::LeapIndicator::LI_NO_WARNING;
}

bool KernelInterface::consoleInputAvailable()
{
	INPUT_RECORD irec;
	DWORD results;
	HANDLE handle = nullptr;

	// GetStdHandle() needs READ_ACCESS
	handle = GetStdHandle(STD_INPUT_HANDLE);

	// PeekConsoleInput() needs READ_ACCESS
	if (PeekConsoleInput(handle, &irec, 1, &results))
	{
		if (results >= 1)
		{
			return true;
		}
	}

	return false;
}

std::uint32_t KernelInterface::hostToNetworkOrder(std::uint32_t figure)
{
	return htonl(figure);
}

std::uint16_t KernelInterface::hostToNetworkOrder(std::uint16_t figure)
{
	return htons(figure);
}

bool KernelInterface::programHasNecessaryRights()
{
	LUID setSystemTimeLUID;

	// lookup the LUID for the SE_SYSTEMTIME privilege
	// What happens, if this privilege does not exists on the system? Is this even possible?
	if (!LookupPrivilegeValue(
	        NULL,
	        SE_SYSTEMTIME_NAME,
	        &setSystemTimeLUID))
	{
		std::string error{"LUID for Privilege '"};
		error += SE_SYSTEMTIME_NAME;
		error += "' not found!";
		LOG_ERROR(error);

		return false;
	}

	HANDLE clientToken = NULL;

	BOOL openresult = OpenProcessToken(GetCurrentProcess(),
	                                   TOKEN_ALL_ACCESS,
	                                   &clientToken);

	if (openresult == FALSE)
	{

		// Get the stupid windows error message.
		char winerrmsg[255];
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		              NULL,
		              GetLastError(),
		              LANG_USER_DEFAULT,
		              winerrmsg,
		              255,
		              NULL);

		LOG_ERROR("Unable to obtain ProcessToken of current process!");
		LOG_ERROR(std::string("Windows error message: ") + winerrmsg);

		return false;
	}

	// Contains the privileges we need. By default, only offers memory for one privilege.
	// To store more than one privilege, you must allocate more memory for that struct.
	TOKEN_PRIVILEGES tp;
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = setSystemTimeLUID;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	// enable the privileges
	if (!AdjustTokenPrivileges(clientToken,
	                           FALSE,
	                           &tp,
	                           0,
	                           (PTOKEN_PRIVILEGES)NULL,
	                           (PDWORD)NULL))
	{
		char winerrmsg[255];

		std::string error{"Unable to enable necessary system privileges!"};
		LOG_ERROR(error);
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		              NULL,
		              GetLastError(),
		              LANG_USER_DEFAULT,
		              winerrmsg,
		              255,
		              NULL);

		LOG_ERROR(std::string("Windows error message: ") + winerrmsg);

		return false;
	}

	// Contains the privileges we need. By default, only offers memory for one privilege.
	// To store more than one privilege, you must allocate more memory for that struct.
	PRIVILEGE_SET setSystemTimePS;

	setSystemTimePS.PrivilegeCount = 1;
	setSystemTimePS.Control = 0;
	setSystemTimePS.Privilege[0].Luid = setSystemTimeLUID;
	setSystemTimePS.Privilege[0].Attributes = SE_PRIVILEGE_ENABLED | SE_PRIVILEGE_USED_FOR_ACCESS;

	BOOL checkResult = FALSE;

	// checks if the privileges are enabled
	if (!PrivilegeCheck(
	        clientToken,
	        &setSystemTimePS,
	        &checkResult))
	{
		char winerrmsg[255];

		std::string error{"Unable to check Privileges!"};
		LOG_ERROR(error);
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		              NULL,
		              GetLastError(),
		              LANG_USER_DEFAULT,
		              winerrmsg,
		              255,
		              NULL);

		LOG_ERROR(winerrmsg);
		return false;
	}

	return checkResult == TRUE;
}

} //End of namespace ntp
