/**
 * @file 	NTP.cpp
 * @details This File implements the NTP Class, which is the entrance point to the program
 * @date 	11.05.2016
 * @author 	Christian Jütte
 * @version	0.1
 *
 * @remarks Just added support for "getMode" for development of the threads.
 * 			The structure of this class is still subject to change.
 */

#include "NTP.hpp"
#include "Log/Logger.hpp"
#include "Thread/ConsoleInterface.hpp"
#include "Thread/Receiver.hpp"
#include "Thread/Synchronizer.hpp"
#include "Thread/Transmitter.hpp"
#include "Utility/ConfigFile.hpp"
#include "Utility/Constants.hpp"
#include "Utility/KernelInterface.hpp"
#include <exception>

ntp::Logger ntp::Logger::instance("ntp_log.txt", LOG_USE_LVL, LOG_USE_CHATTYFLAGS);
ntp::ConfigFile ntp::ConfigFile::instance("config/ntp/config.ini");
ntp::Constants ntp::Constants::instance;
ntp::KernelInterface ntp::KernelInterface::instance;
NtsUnicastService ntp::NTP::nts;

namespace ntp
{

NTP::AppMode NTP::mode = NTP::AppMode::MODE_SERVER;
std::exception_ptr NTP::exceptionPtr;

/**
 * @brief	Constructor of NTP
 * @details	The constructor of NTP is the entrance to the ntp-application.
 * 			This method sets the mode (from the config.ini) in the program.
 * 			It also starts the Threads, which are recommend for the dedicating mode (Server or Client).
 * 			<ul>
 * 			<li><b>Server-Mode:</b> Here is only the Receiver-Thread recommend. The Transmitter-Class supplies only a fastTransmit()-method
 * 							 to sendout a received and validated packet from a client.</li>
 * 			<li><b>Client-Mode:</b> In this mode are all threads recommend to start. The client is stateful and realizes the synchronize process
 * 							 on the computer.</li>
 * 			</ul>
 * @see		Transmitter.cpp, fastTransmit(ntp::Message msg)
 */
NTP::NTP()
{

#ifdef _DEBUG
	INFO("NTPv4 (rfc 5905, NTPv3 not supported) v" + NTP_VERSION + " (" + NTP_COMPILE_TIME + ") - debug build");
#else
	INFO("NTPv4 (rfc 5905, NTPv3 not supported) v" + NTP_VERSION + " (" + NTP_COMPILE_TIME + ") - release build");
#endif

	mode = NTP::AppMode::MODE_SERVER;

	ConfigFile& config = ConfigFile::getInstance();

	if (!config.existConfigFile())
	{
		LOG_ERROR("Can't access the config-file " + config.getConfigFile());
		exit(1);
	}

	if (!config.isConfigFileValid())
	{
		LOG_ERROR("The config-file " + config.getConfigFile() + " uses an invalid encoding. Only ANSI is supported currently.");
		exit(1);
	}

	setAppModeFromConfig(config);

	if (!KernelInterface::programHasNecessaryRights())
	{
		LOG_ERROR("The program was not run with the necessary rights. Exiting!");
		exit(1);
	}

	INFO(std::string("NTP started, NTP Mode: ") +
	     (mode == NTP::AppMode::MODE_SERVER ? "Server" : "Client"));

	// later because another logging-mode may suppress important log messages from above
	setLoggingDir(config);
	setLoggingModes(config);
	loadLoggingMode(config);

	KernelInterface& ki = KernelInterface::getInstance();

	//First, check if another process of the same kind is already running.
	//This also initializes shared memory
	if (ki.isOtherProcessRunning())
	{
		//Get mode
		std::string modeStr;
		switch (mode)
		{
		case MODE_CLIENT:
			modeStr = "Client";
			break;
		case MODE_SERVER:
			modeStr = "Server";
			break;
		}
		std::string error = "Another NTP-";
		error.append(modeStr);
		error.append(" is already running. Exiting the application.");
		LOG_ERROR(error);
		throw(ntp::FileException(error, true));
	}

	//Initialize the system variables to default values
	ki.updateSystemVariables(SharedSystemVariables());

	// initialize connection
	try
	{
		ki.initSocket();
	}
	catch (ntp::NetworkException& ex)
	{
		LOG_ERROR(ex.toString());
		exit(1);
	}

	bool useNTS = false;
	if (config.readBool(useNTS, "ClientMode", "UseNTS", false))
	{
		DEBUG(std::string("config: UseNTS successfully read: UseNTS=") + std::to_string(useNTS));
	}
	else
	{
		WARN("UseNTS not found in config. Not using NTS.");
	}

	// use NTS in server-mode or if wanted in client-mode
	if (getMode() == MODE_SERVER || (getMode() == MODE_CLIENT && useNTS))
	{
		try
		{
			// NTS Unicast
			nts.initialize("./config/nts/global.ini");
			nts.showInfos();
		}
		catch (std::string const& s)
		{
			LOG_ERROR(s);
		}
		catch (std::exception& e)
		{
			LOG_ERROR("nts exception: " + e.what());
		}
		catch (...)
		{
			LOG_ERROR("An unknown exception occured during nts initialization");
			exit(1);
		}
	}

	bool consoleUserIO = false;
	config.readBool(consoleUserIO, "GeneralSettings", "ConsoleUserIO", false);

	// start program depending on mode
	switch (mode)
	{
	case MODE_CLIENT:
	{
		try
		{

			Transmitter transmThread;
			Receiver recThread(transmThread);
			Synchronizer syncThread(recThread, transmThread);

			std::thread t1(std::ref(transmThread));
			std::thread t2(std::ref(recThread));
			std::thread t3(std::ref(syncThread));

			// Start thread for console I/O
			if (consoleUserIO)
			{
				ConsoleInterface ioThread(recThread, transmThread, syncThread);
				std::thread t4(std::ref(ioThread));
				t4.join();
			}

			t1.join();
			t2.join();
			t3.join();

			std::rethrow_exception(exceptionPtr);
		}
		catch (const std::exception& e)
		{
			//WARN("Exception thrown.");
			INFO("Application ended due to the following reason: " + e.what());
		}
		break;
	} // end of case Client

	case MODE_SERVER:
	{
		try
		{
			Transmitter transmitter;
			Receiver recThread(transmitter);
			ConsoleInterface ioThread(recThread);

			std::thread t1(std::ref(recThread));

			if (consoleUserIO)
			{
				std::thread t2(std::ref(ioThread)); // Start thread for console I/O
				t2.join();
			}

			t1.join();

			std::rethrow_exception(exceptionPtr);
		}
		catch (const std::exception& e)
		{
			//WARN("Network Exception thrown.");
			INFO("Application ended due to the following reason: " + e.what());
		}
		break;
	} // end of case Server

	} // end of switch()
}

/**
 * @brief Destructor of NTP
 * @details Do not do anything.
 */
NTP::~NTP()
{
	//Free all memory etc.
}

/**
 * @brief Getter method, gives the mode of the application.
 * @details With this method, you can get information about the mode of the application (Client, Server).
 */
NTP::AppMode NTP::getMode()
{
	return mode;
}

/**
 * @brief	sets the exception pointer
 *
 */
void NTP::setExceptionPtr(std::exception_ptr exceptionPtr)
{
	NTP::exceptionPtr = exceptionPtr;
}

/**
 * @brief	returns the exception_ptr
 *
 */
std::exception_ptr NTP::getExceptionPtr()
{
	return NTP::exceptionPtr;
}

/**
 * @brief	returns the instance of NtsUnicastService to use with NtsInterface
 *
 */
NtsUnicastService& NTP::getNtsUnicastService()
{
	return NTP::nts;
}

/**
 * @brief 	sets the AppMode depending on the given configuration
 * @details	It reads the configured mode from the key "AppMode" in section "GeneralSettings"
 * 			in the ConfigFile.
 * 			If the AppMode is not set in the configuration, this method will cause the
 * 			application to exit.
 */
void NTP::setAppModeFromConfig(ConfigFile& config)
{
	std::string configAppMode;

	if (!config.readString(configAppMode, "GeneralSettings", "AppMode", ""))
	{
		LOG_ERROR("Failed to read AppMode from ConfigFile!");
	}

	if (configAppMode == "")
	{
		LOG_ERROR("Failed to read AppMode from ConfigFile!");
		exit(1);
	}
	else if (configAppMode == "CLIENT")
	{
		DEBUG("Loaded from config: program running as client");
		mode = NTP::AppMode::MODE_CLIENT;
	}
	else if (configAppMode == "SERVER")
	{
		DEBUG("Loaded from config: program running as server");
		mode = NTP::AppMode::MODE_SERVER;
	}
}

/**
 * @brief	Loads flags from configuration and configures the logging interface.
 * @details	When executing this method, it will read the booleans ConsoleLogging
 * 			and FileLogging from the given configuration.
 *
 * 			If ConsoleLogging is set to true, logging information will be directed
 * 			to the console. If FileLogging is set to true, the output will be directed
 * 			to the log file.
 */
void NTP::setLoggingModes(ConfigFile& config)
{
	Logger& log = Logger::getInstance();

	bool consoleLoggingEnable = true;

	if (!config.readBool(consoleLoggingEnable, "GeneralSettings", "ConsoleLogging", true))
	{
		DEBUG("Failed to read consoleLoggingEnable from ConfigFile! Console logging enabled.");
	}
	log.enableConsoleOutput(consoleLoggingEnable);

	bool fileLoggingEnable = true;

	if (!config.readBool(fileLoggingEnable, "GeneralSettings", "FileLogging", false))
	{
		DEBUG("Failed to read fileLoggingEnable from ConfigFile! Logging to file disabled.");
	}
	log.enableFileOutput(fileLoggingEnable);
}

/**
 * @brief	Reads desired logging mode from config and configures logging module
 * @details	If there is no level defined in the configuration file or if it is not
 * 			readable, "DEBUG" will be used as default value.
 */
void NTP::loadLoggingMode(ConfigFile& config)
{
	Logger& log = Logger::getInstance();
	std::string loggingMode = "";

	if (!config.readString(loggingMode, "GeneralSettings", "LoggingMode", "DEBUG"))
	{
		DEBUG("Failed to read LoggingMode from ConfigFile! Mode was set to DEBUG.");
	}

	int loglevel = 0;
	try
	{
		loglevel = Logger::logLevelFromString(loggingMode);
	}
	catch (std::invalid_argument& e)
	{
		DEBUG(std::string("No loglevel found for LoggingMode: ") + loggingMode +
		      " -> Using DEBUG as default mode.");
		loglevel = LOG_DEBUG_LVL;
	}

	log.setLogLevel(loglevel);
}

/**
 * @brief	Sets the directory, where the log file should be stored.
 * @details	It loads the directory from the configuration and sets the directory in
 * 			the logger.
 */
void NTP::setLoggingDir(ConfigFile& config)
{

	Logger& log = Logger::getInstance();
	std::string loggingDir = "";

	if (!config.readString(loggingDir, "GeneralSettings", "LogDirectory", ""))
	{
		DEBUG("Failed to read LogDirectory from ConfigFile!");
	}
	else
	{
		log.setLogDir(loggingDir);
	}
}

} //End of namespace ntp
