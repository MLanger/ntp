/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file Transmitter.hpp
 * @brief ---
 * @details ---
 *
 * @date 2016
 * @author Simon Häußler
 * @version 0.1
 *
 * @copyright Copyright 2016, Simon Häußler.
 * 			  All rights reserved.
 */

#ifndef NTP_THREAD_TRANSMITTER_HPP_
#define NTP_THREAD_TRANSMITTER_HPP_

#include "../Datatypes/Frame/Message.hpp"
#include "../Datatypes/Frame/NTPPacket.hpp"
#include "../Datatypes/Peer/Peer.hpp"
#include "../Log/Logger.hpp"
//#include "../NtsInterface.hpp"
#include "../Utility/ConfigFile.hpp"
#include "../Utility/KernelInterface.hpp"
#include "../Utility/target_specific_thread.hpp"
#include <queue>
//#include <mutex>
#include "../Utility/target_specific_mutex.hpp"
#include <iostream>
#include <list>

namespace ntp
{

/**
 * @brief Class Transmitter is one of the three main-threads. It processes the transmission of NTP messages.
 * @details This class is a functor. This means an object f this class can give to an Thread-constructor. The thread will
 * 			execute the function declared with the function operator()(). It realizes the transmit process in the NTP-software.
 * @date 28.03.2016
 * @author Simon Häußler
 * @version 0.1
 */
class Transmitter
{

private:
	//private attributes
	/**
	 * @brief Queue for the different transmit tasks.
	 * @details This queue rests upon the FIFO (First In First Out) concept. The first transmit task in this queue will transmit at first.
	 * 			Every new transmit task will push at the back of the queue.
	 */
	std::queue<ntp::Message> transmitQueue;

	/**
	 * @brief Mutex for the transmitter to make it thread save.
	 * @details This mutex locks this Thread thereby two other Threads do not use a function of this class simultaneous.
	 */
	static std::mutex transmitterMutex;

	///@name Constants - loaded from config
	///@{
	bool USE_NTS;
	bool REFERENCE_CLOCK;
	char REFERENCE_ID[4];
	int NTS_FIELD_TYPE;
	signed char POLL;
	///@}

	//private methods
	void transmitProcess_Client(void);

public:
	//public functions
	Transmitter(void);
	Transmitter(const Transmitter&) = delete;
	~Transmitter(void);
	void addTransmitTask(ntp::Peer&);
	void fastTransmit_Client(ntp::Message);
	void fastTransmit_Server(ntp::Message);
	bool hasQueueMessage();
	ntp::Message getMessageFromTransmitQueue();
	void operator()(void);

}; //end of class Transmitter

} //end of namespace ntp

#endif /* NTP_THREAD_TRANSMITTER_HPP_ */
