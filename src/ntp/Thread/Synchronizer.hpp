/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	Synchronizer.hpp
 * @details This Header declares the Synchronizer Class.
 * @date 	22.06.2016
 * @author 	Christian Jütte
 * @version	0.1
 */

#ifndef NTP_THREAD_SYNCHRONIZER_HPP_
#define NTP_THREAD_SYNCHRONIZER_HPP_

#include "../Datatypes/Exceptions/FileException.hpp"
#include "../Datatypes/Exceptions/TimeException.hpp"
#include "../Datatypes/IPAddress.hpp"
#include "../Datatypes/SelectionTuple.hpp"
#include "../Datatypes/SharedSystemVariables.hpp"
#include "../Datatypes/SurvivorCandidate.hpp"
#include "../Utility/ConfigFile.hpp"
#include "../Utility/KernelInterface.hpp"
#include "../Utility/target_specific_thread.hpp"
#include "Receiver.hpp"
#include "Transmitter.hpp"
#include <chrono>
//#include "../NTP.hpp"

//#define SYNC_MIN_SURVIVORS 3		///<NMIN
//#define SYNC_MIN_CANDIDATES 1		///<CMIN

#define SYNC_MIDPOINT 0
//#define SYNC_STEP_THRESHOLD 0.125
//#define SYNC_PANIC_THRESHOLD 1000

#define SYNC_POLL_LIMIT 30 ///<Poll-Adjust Threshold
#define SYNC_POLL_GATE 4   ///<Poll-Adjust Gate

namespace ntp
{

/**
 * @brief	This defines the Synchronizer class, which is one of the three main threads in client mode.
 * 			It serves to calculate the offset from all Peers and to adjust the system clock.
 *
 * ## General Description
 *
 * The Synchronizer is one of the three main threads in client mode.
 * Together with the classes Peer, PacketStatistics, SelectionTuple,
 * SharedSystemVariables and SurvivorCandidate it manages the
 * Peer Process (including the Clock Filter Algorithm), the System Process
 * (Select, Cluster and Combine Algorithms) and the Poll Process as defined
 * in RFC 5905. As a result it invokes the Clock-Adjust-Process, which is
 * executed by the functions setTime and adjustTime from the KernelInterface.
 *
 * ## Operation
 *
 * On construction, the Synchronizer loads the list of peers (i.e. servers to synchronize to)
 * from the config-file and initializes the peerList. Each Peer is initialized with
 * default-values. Peers can be loaded either by IPv4-Address or hostname.
 *
 * The thread is started in NTP.cpp using the object as a function (with operator()()).
 * The thread then executes the function syncProcess(), which keeps going until
 * an exception occurs or the application is terminated.
 *
 * In syncProcess() the individual Peers are queried whether they are ready to be polled.
 * If this is the case, a new transmit-task is issued to the Transmitter, which will
 * then send out a packet to the designated Peer.
 *
 * On each iteration of it's while-loop the syncProcess()-function also checks for
 * newly received messages. It then takes the new messages from the Receiver and
 * forwards them to the corresponding Peers, which process the new
 * information. After processing all new messages syncProcess() runs
 * the Select, Cluster and Combine Algorithms in order to eventually synchronize
 * the system clock.
 *
 * The Synchronizer also updates the System Variables, which are shared with a
 * program configured as Server on the same system.
 *
 */

class Synchronizer
{

private:
	///A file to log system statistics (offset, jitter)
	LogFile systemStatLog;
	///Flag to tell whether logging the stats of the System is enabled. Is set by config on construction.
	bool systemStatLogEnabled = true;

	///A list of all Peers on the system. Using pointers here since we can't copy Peers. This list is sorted by address.
	std::vector<ntp::Peer*> peerList;

	///The candidate List used in the select algorithm to find the truechimers.
	std::vector<ntp::SelectionTuple> candidateList;

	///The survivor List used in the cluster algorithm.
	std::vector<ntp::SurvivorCandidate> survivorList;

	///Object of the KernelInterface
	ntp::KernelInterface& kernelInterface;

	///Object of the current Receiver
	ntp::Receiver& receiver;

	///Object of the current Transmitter
	ntp::Transmitter& transmitter;

	///Specifies wether time is manipulated or not. Can be changed through a console command. This only affects the pure adjustment, not the poll process, so if this is false, packets are still being sent.
	bool syncEnabled = true;

	/**
	 * @name 	System Variables
	 * @brief 	The Synchronizer manages all system variables.
	 * @details These are the system variables as specified in RFC5905 (p. 68),
	 * and their corresponding variables in this implementation.
	 *
	 *	Variable of reference implementation 	| Meaning 			| Corresponding variable in this implementation
	 *	---------------------------------------	|-------------------|-----------------------------------------
	 *	tstamp  t;								|update time		| lastSync
	 *	char    leap;							|leap indicator		| kernelInterface.getLeapIndicator()
	 *	char    stratum;						|stratum			| systemVariables.stratum
	 *	char    poll;							|poll interval		| systemVariables.poll
	 *	char    precision;						|precision			| kernelInterface.getSystemPrecision()
	 *	double  rootdelay;						|root delay			| systemVariables.rootDelay
	 *	double  rootdisp;						|root dispersion	| systemVariables.rootDispersion
	 *	char    refid;							|reference ID		| -
	 *	tstamp  reftime;						|reference time		| systemVariables.referenceTimestamp
	 *	struct m m[NMAX];						|chime list			| candidateList
	 *	struct v v[NMAX];						|survivor list		| survivorList
	 *	struct p *p;							|association ID		| survivorList[0].peer
	 *	double  offset;							|combined offset	| systemOffset
	 *	double  jitter;							|combined jitter	| systemJitter
	 *	int     flags;							|option flags		| -
	 *	int     n;								|number of survivors| survivorList.size()
	 */
	///@{

	///Offset that was computed by the select, cluster and combine algorithms that is used to discipline the system clock.
	double systemOffset;

	///The system jitter as determined by the cluster and combine algorithm.
	double systemJitter;

	///The selection Jitter, which is a product of the cluster algorithm and used in the combine algorithm.
	double selectionJitter;

	///System Variables that are shared with the server process
	SharedSystemVariables systemVariables;

	///Time of the receipt of the last message that was synchronized to.
	std::chrono::steady_clock::time_point lastSync;

	///Point in time when the Synchronizer last tried to poll the Peers. Used to make the loop go every second.
	std::chrono::steady_clock::time_point lastPoll;

	///Is true when the client has been synchronized at least once (i.e., the time has been set).
	bool synchronized = false;

	///@}

	int jiggleCount = 0; ///<Jiggle Counter. Used to adjust the system poll interval

	///@name Constants - loaded from configuration file
	///@{
	int MIN_CANDIDATES; ///< Minimum number of candidates. CMIN
	int MIN_SURVIVORS;  ///< Minimum number of survivors. NMIN
	bool PANIC_ENABLED;
	int PANIC_THRESHOLD;
	double STEP_THRESHOLD;
	///@}

	bool select();
	void cluster();
	void combine();
	void syncTime();
	void syncProcess();
	bool getPeerByAddress(IPAddress peerAddress, ntp::Peer** peer);
	bool loadPeersFromConfig();
	bool addPeerByHostname(std::string hostname);
	Peer& addPeerByAddress(IPAddress peerAddress);
	double calcRootSyncDistance(Peer& peer);

public:
	Synchronizer(ntp::Receiver& receiver, ntp::Transmitter& transmitter);
	/// The copy-constructor is explicitly deleted since we don't want to copy the Synchronizer
	Synchronizer(const Synchronizer&) = delete;
	~Synchronizer();
	void operator()();
	ntp::LongTimestamp getReferenceTimestamp();
	double getSystemJitter();
	bool isSynchronized();
	void setSync(bool state);

}; //End of class Synchronizer

} //End of namespace ntp

#endif /* NTP_THREAD_SYNCHRONIZER_HPP_ */
