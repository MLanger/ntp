/**
 * @file 	LongTimestamp.cpp
 * @details NTP Receiver thread definition.
 * @date 	01.05.2016
 * @author 	silvan
 * @version	1
 */

#include "Receiver.hpp"
#include "../Datatypes/Exceptions/TimeoutException.hpp"
#include "../Datatypes/Frame/NTPPacket.hpp"
#include "../Log/Logger.hpp"
#include "../NTP.hpp"
#include <NtsUnicastService.h>
#include <memory>
#include "NtpPacketInfo.h"


namespace ntp
{


const char* Receiver::TooManyPacketsInQueue::what(void) const noexcept
{
	return "Cant add more packets to receiveQueue. Maximum reached.";
}

/**
 * @brief	Constructor for Client Mode
 * @details	Takes a reference to the transmitter for adding transmit tasks.
 */
Receiver::Receiver(Transmitter& transmitter) : transmitter(&transmitter)
{
}

/**
 * @brief	does nothing
 */
Receiver::~Receiver()
{
}

/**
 * @brief	Used for thread
 * @details	This method makes the object a function-object, which is callable
 * 			by class std::thread. It behaves different depending on the programs
 * 			mode (either server or client). It catches all exceptions, gives them
 * 			to the NTP class by calling NTP::setExceptionPtr(). After an exception
 * 			has been caught, the execution of this thread will end.
 * @see		receiveProcessClient() receiveProcessServer()
 */
void Receiver::operator()(void)
{
	while (!(NTP::getExceptionPtr()))
	{
		try
		{

			if (NTP::getMode() == NTP::MODE_SERVER)
			{
				receiveProcessServer();
			}
			else if (NTP::getMode() == NTP::MODE_CLIENT)
			{
				receiveProcessClient();
			}
		}
		catch (...)
		{
			//			LOG_ERROR(std::string("Error in Receiver!") + std::current_exception()->what() );
			DEBUG("ReceiverError. Setting exception pointer.");
			NTP::setExceptionPtr(std::current_exception());
			break;
		}
	}
}

/**
 * @brief	full receive Process in client-mode
 * @details	This method is called once and should execute in a separate thread. It will run
 * 			indefinitely (while(true)) and process incoming messages which are fetched by
 * 			calling KernelInterface::receivePacket().
 *
 * 			Once a packet has been fetched, it will be validated. If valid, the received message
 * 			is added to the receiveQueue to become processed by other threads. Invalid packets
 * 			will be discarded.
 */
void Receiver::receiveProcessClient(void)
{
	// sequence according to the diagram made by Christian (v03)

	KernelInterface& ki = KernelInterface::getInstance();
	ConfigFile& config = ConfigFile::getInstance();

	// read out the state of the use of NTS
	bool useNTS = false;
	config.readBool(useNTS, "ClientMode", "UseNTS", false);

	INFO("started receive process in client mode");

	while (!(NTP::getExceptionPtr()))
	{
		std::shared_ptr<Message> msg;

		try
		{
			msg = ki.receivePacket(); // blocks until a packet has arrived
		}
		catch (TimeoutException& t)
		{
			CHATTY_POLL_PROCESS("receivePacket() timed out");
			continue;
		}
		catch (InvalidPacketException& e)
		{
			LOG_ERROR("Received Invalid Packet!" + e.toString());
			continue;
		}

		DEBUG(std::string("received packet: ") + msg->getPayload().toString());

		if (!validatePacket(msg))
		{
			// discard packet if invalid
			WARN("got invalid packet from server - discarding!");
			continue; // continue with waiting for next packet
		}

		// else: packet is valid
		if (!useNTS)
		{
			// NTS is inactive, the received packet is added to the receive queue
			try
			{
				addMsgToQueue(*msg);
				DEBUG("added message to ReceiveQueue [noNTS]");
			}
			catch (TooManyPacketsInQueue& e)
			{

				WARN("Unable to add more Packets. Queue is full. Discarding packet.");
			}
		}
		else
		{
			//NTS is active, the validation in fastTransmit could start
			NTPPacket & ntpPacket = msg->getPayload();
			std::string IpAddress = msg->getPeerAddress().toStringNoPort();
			std::vector<unsigned char> rawNtp = ntpPacket.createFrame();
			int ntpLen = rawNtp.size();
			bool status = true;

			NtpPacketInfo pkifo;
			pkifo.setServer(IpAddress.c_str(), IpAddress.c_str(), 123);


			// verify received NTS message
			
			// NTS-Nachricht überprüfen und parsen
			int result = ntp::NTP::getNtsUnicastService().processAndVerify(pkifo, rawNtp.data(), ntpLen, rawNtp.size());

			if (result < 0)
			{
				printf("[FAIL] 'NTS_process_and_verify()':  error code: %d\n", result);
				status = false;
			}
			else
			{
				printf("[SUCCESS] 'NTS_process_and_verify()': current NTS content size: %d bytes\n", result);
			}

			ntpPacket.loadFromUDP(rawNtp);


			//check if the packet can use for time synchronization
			if (status == true)
			{
				try
				{
					addMsgToQueue(*msg);
					DEBUG("added message to ReceiveQueue [withNTS]");
				}
				catch (TooManyPacketsInQueue& e)
				{

					WARN("Unable to add more Packets. Queue is full. Discarding packet.");
				}
			}
		}
	}

	INFO("stopped receive process in client mode");
}

/**
 * @brief	full receive process in server-mode
 * @details	This method is called once and should execute in a separate thread. It will run
 * 			indefinitely (while(true)) and process incoming messages which are fetched by
 * 			calling KernelInterface::receivePacket().
 *
 * 			Once a packet has been fetched, it will be validated. If valid, the response
 * 			(including ReceiveTimestamp and TransmitTimestamp) will be transmitted to the
 * 			Peer who sent the request. Invalid packets will be discarded.
 */
void Receiver::receiveProcessServer(void)
{
	// sequence according to the diagram made by Christian (v02)

	KernelInterface& ki = KernelInterface::getInstance();

	INFO("started receive process in server mode");

	while (!(NTP::getExceptionPtr()))
	{
		std::shared_ptr<Message> msg;

		try
		{
			msg = ki.receivePacket(); // blocks until a packet has arrived
		}
		catch (TimeoutException& t)
		{
			CHATTY_POLL_PROCESS("receivePacket() timed out");
			continue;
		}
		catch (InvalidPacketException& e)
		{
			LOG_ERROR("Received Invalid Packet!" + e.toString());
			continue;
		}
		// set ReceiveTimestamp? (if not done by Message() before)

		if (!validatePacket(msg))
		{
			// discard packet if invalid
			WARN("got invalid packet from client - discarding!");
			WARN(msg->getPayload().toString());
			continue; // continue with waiting for next packet
		}

		DEBUG("got valid packet:\n" + msg->getPayload().toString());

		// send message to peer
		try
		{
			// NTS-Validation is done in fastTransmit() if NTS is active
			transmitter->fastTransmit_Server(*msg);
		}
		catch (NetworkException& e)
		{
			LOG_ERROR(e.toString());
		}
	}

	INFO("stopped receive process in server mode");

} // end receiveProcessServer()

/**
 * @brief	returns the oldest received message from the queue and deletes it
 * @details	This method will lock the receiverMutex, copy the oldest element from the queue,
 * 			remove it and finally unlock again.
 * @note	Locking the receiverMutex means that this method will block when the mutex has
 * 			been locked already (e.g. by another Thread)!
 * @see		addMsgToQueue
 */
Message Receiver::getOldestMessage(void)
{
	std::lock_guard<std::mutex> _lg(receiverMutex);

	Message m = receiveQueue.front();
	receiveQueue.pop();
	return m;
}

/**
 * @brief	checks whether there is a new message in the queue
 * @details	Due to different threads performing read- and write-operations on the queue
 * 			this value could have changed directly after you called this method! Make
 * 			sure that this method and getOldestMessage() will never be called in different
 * 			threads.
 */
bool Receiver::isMessageAvailable(void)
{
	return (receiveQueue.size() > 0);
}

/**
 * @brief	adds a received message to the queue
 * @details	This method will lock the receiverMutex, add the given msg to the queue and
 * 			finally unlock again.
 * @note	Locking the receiverMutex means that this method will block when the mutex has
 * 			been locked already (e.g. by another Thread)!
 * @see		getOldestMessage
 * @remarks	Use shared_ptr to avoid time consuming copy-operations?
 */
void Receiver::addMsgToQueue(const Message& msg)
{
	std::lock_guard<std::mutex> _lg(receiverMutex);

	if (receiveQueue.size() >= MAX_PACKETS_IN_QUEUE)
	{
		throw TooManyPacketsInQueue();
	}
	else
	{
		receiveQueue.push(msg);
	}
}

/**
 * @brief	validates a provided NTPPacket
 * @details	This method performs the following validation checks on the NTPPacket:
 * 			- test for the right version (as set in Constants)
 * 			- check if the packet has got the right mode, depending on the mode of
 * 			  the program
 * 			- check if the right timestamps are set, depending on the mode of the packet
 * @warning	actually only testing for the right version, the packet mode and timestamps is supported!
 */
bool Receiver::validatePacket(std::shared_ptr<Message> msg) const
{
	// right version?
	NTPPacket const& pack = msg->getPayload();

	if (pack.getVersion() < MIN_NTP_VERSION || pack.getVersion() > MAX_NTP_VERSION)
	{
		return false;
	}

	// test if packet mode is processable in general for this implementation
	if (pack.getMode() != NTP_MODE_SERVER && pack.getMode() != NTP_MODE_CLIENT)
	{
		WARN("Packet has unsupported mode.");
		return false;
	}

	// if this is the client, packet must be from server
	if (NTP::MODE_CLIENT == NTP::getMode() && pack.getMode() == NTP_MODE_SERVER)
	{
		DEBUG("Got packet from server, we are client -> everything is fine.");
	}
	// if this is the server, packet must be from client
	else if (NTP::MODE_SERVER == NTP::getMode() && pack.getMode() == NTP_MODE_CLIENT)
	{
		DEBUG("Got packet from client, we are server -> everything is fine.");
	}
	else
	{
		WARN("Program is not in the right mode to process that packet. Maybe you set the wrong mode?");
		return false;
	}

	// check whether all necessary timestamps are set (depending on mode)
	if (pack.getMode() == NTP_MODE_CLIENT)
	{
		// do things for client
		if (0.0 == pack.getTransmitTime())
		{
			WARN("Transmit timestamp is zero.");
			return false;
		}
	}
	else if (pack.getMode() == NTP_MODE_SERVER)
	{
		if (0.0 == pack.getReferenceTime())
		{
			WARN("Reference timestamp set to zero!");
			return false;
		}
		if (0.0 == pack.getOriginTime())
		{
			WARN("Origin timestamp set to zero!");
			return false;
		}
		if (0.0 == pack.getTransmitTime())
		{
			WARN("Transmit timestamp set to zero!");
			return false;
		}
	}

	return true;
}

} // end of namespace ntp
