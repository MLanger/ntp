/**
 * @file Transmitter.cpp
 * @see Transmitter.hpp
 *
 * @date 2016
 * @author Simon Häußler
 * @version 0.1
 * @copyright Copyright (c) 2016, Simon Häußler.
 * 			  All rights reserved.
 */

#include "Transmitter.hpp"
#include "../NTP.hpp"
#include "../Utility/ConfigFile.hpp"
#include "NtpPacketInfo.h"
#include <algorithm>    // std::replace
#include <iostream>
#include <sstream>

namespace ntp
{


// initialize the mutex to control accesses on the functions
std::mutex Transmitter::transmitterMutex;

/**
 * @brief 	This is the Constructor of the Transmitter function-class.
 * @details <br>In <strong>Client Mode</strong> it creates a transmission queue with space
 *			to add Messages to send out to a NTP or NTS server. To choose the right messages it
 *			read out the usage of the NTS service of the configuration file.
 *			<br>In <strong>Server Mode</strong> the Transmitter don't need any queue for Messages to send out, because
 *			the server immediately respond to the client if a message arrives. In this case, the constructor loads
 *			some data from the configuration file:
 *			<ul>
 *			<li>Reference Clock: This is a flag to simulate that this server is a stratum 2 server.</li>
 *			<li>Poll Interval: If the server is a stratum 2 server, it have to set the Poll Interval at the response Messages
 *			to tell the client the poll interval he have to use.</li>
 *			<li>Reference ID: If the server is a stratum 2 server, it have to know the reference clock type given in this ID.</li>
 *			<li>NTS Field Type: With this number the server can recognize if a Message with NTS content in its Exentsion Field(s) arrives.</li>
 *			</ul>
 */
Transmitter::Transmitter(void)
{
	ConfigFile& config = ConfigFile::getInstance();

	switch (NTP::getMode())
	{
	case ntp::NTP::AppMode::MODE_SERVER:
	{
		//Loading variables from config
		if (!config.readBool(REFERENCE_CLOCK, "ServerMode", "ReferenceClock", false))
			WARN("Failed to read ReferenceClock from config-file. Setting to default.");

		int pollInt = 0;
		if (!config.readInt(pollInt, "ServerMode", "Poll", 6))
			WARN("Failed to read Poll from config-file. Setting to default.");

		//Poll must be clamped to PollMin - PollMax
		Constants& C = Constants::getInstance();
		unsigned char POLL_MAX = C.getUChar(ConstantsUChar::pollMax);
		unsigned char POLL_MIN = C.getUChar(ConstantsUChar::pollMin);
		if (pollInt > POLL_MAX)
		{
			POLL = POLL_MAX;
		}
		else if (pollInt < POLL_MIN)
		{
			POLL = POLL_MIN;
		}
		else
		{
			POLL = static_cast<signed char>(pollInt);
		}

		std::string configRefID = ""; // create a string to store the reference ID

		// read the value of the reference ID out of the configuration file
		if (!config.readString(configRefID, "ServerMode", "ReferenceID", ""))
		{
			WARN("Failed to read ReferenceID from config-file.");
		}

		// verify the value of the read reference ID
		if (configRefID == "")
		{
			WARN("Failure in configuration file: ReferenceID not set.");
			for (int i = 0; i < 4; i++)
			{
				REFERENCE_ID[i] = 0;
			}
		}
		else
		{
			// copy the value into a char array
			for (unsigned int i = 0; i < 4; i++)
			{
				if (i < configRefID.length())
					REFERENCE_ID[i] = configRefID[i];
				else
					REFERENCE_ID[i] = 0;
			}
		}

		if (!config.readInt(NTS_FIELD_TYPE, "ServerMode", "NTSFieldType", 1024))
			WARN("Failed to read NTSFieldType from config-file.  Setting to default (1024)."); // log the reading error
		break;
	}

	case ntp::NTP::AppMode::MODE_CLIENT:
	{
		//Loading variables from config
		// read the useNTS flag out of the configuration file
		if (!config.readBool(USE_NTS, "ClientMode", "UseNTS", false))
			WARN("Failed to read the useNTS flag from config-file. Running without NTS!"); // log the reading error

		transmitQueue = std::queue<ntp::Message>(); // create queue to control order

		DEBUG("Transmitter and TransmitQueue created.");
		break;
	}

	default:
	{
		WARN("Create a ntp::Transmitter object in unknown mode!");
	}
	}
}

/**
 * @details Deletes the allocated  memory.
 */
Transmitter::~Transmitter(void)
{
	DEBUG("Cleanup of Transmitter done.");
}

/**
 * @brief Adds a transmit-task to the transmit-queue as a Message object.
 * @details The addTransmitTask() function can be used to add a Message object as a transmit task to the transmit queue.
 * 			The current data of the Packet and the dedicating Peer are stored in the added Message object.
 * 			<br>If the program works in client-mode, the Message object is here only filled with the Leap Indicator,
 * 			the Packet Mode and the Version Number (explained in the following enumeration). If it works in server-mode,
 * 			the Message object will fill with the important informations of the reference clock (peer),
 * 			listed in the following enumeration:<br>
 * 			<ul>
 * 			<li>Leap Indicator (leap): <br>Warning of an impending leap second to be inserted or deleted in the last minute of the current month.<br>
 * 										  @see enum LeapIndicator of namespace ntp.</p></li>
 * 			<li>Version Number (version): <p>Value representing the NTP version number, currently 4.</li>
 * 			<li>Mode (mode): <p>Value representing the mode of the transmitted packet.@see enum NTP_MODE of namespace ntp.</p></li>
 * 			<li>Stratum (stratum): <p>Value representing the stratum of the actual reference clock. Values are defined in the following table:<br>
 * 									  Value  | Meaning
 * 									  -------|-----------------------------------------------------
 * 									  0 	 | unspecified or invalid
 * 									  1 	 | primary server (e.g., equipped with a GPS receiver)
 * 									  2-15   | secondary server (via NTP)
 * 									  16 	 | unsynchronized
 * 									  17-255 | reserved
 * 									  </p></li>
 * 			<li>Poll: <p>Value representing the maximum interval between successive messages. Unit: log2 seconds.</p></li>
 * 			<li>Precision: <p>Value representing the precision of the actual reference or system clock. Unit: log2 seconds.</p></li>
 * 			<li>Root Delay (rootdelay): <p>Total round-trip delay (RTT) to the reference clock (root of the NTP-network). In NTP short format.</p></li>
 * 			<li>Root Dispersion (rootdisp): <p>Total dispersion to the reference clock (root of the NTP-network). In NTP short format.</p></li>
 * 			<li>Reference ID (refid): <p>This is the "[KissCode]" if stratum is 0. If stratum is 1, it is a four-character ASCII string identifying
 * 			the particular server or reference clock. For any different stratum, it returns "[IPAddr]".</p></li>
 * 			</ul>
 * 			<br>Process of this function:<br>
 * 			This function locks a mutex with the lock-guard of std to be thread-save. The lock guard ensures to unlock the mutex.
 *
 * @see NTPPacket.hpp
 *
 * @param peer The Peer object of the asking client.
 */
void Transmitter::addTransmitTask(ntp::Peer& peer)
{
	//if the program quits or an exception occurs, the lock guard ensures the unlocking of the mutex.
	std::lock_guard<std::mutex> lg(this->transmitterMutex);

	switch (NTP::getMode())
	{

	//IF MODE of Host = CLIENT
	case NTP::AppMode::MODE_CLIENT:
	{
		// create dummy NTPPacket
		ntp::NTPPacket tempPacket = ntp::NTPPacket();

		// set significant values in the packet @see RFC5905
		tempPacket.setVersion(4);
		tempPacket.setMode(NTP_MODE_CLIENT);

		// create dummy Message object
		ntp::Message msg = ntp::Message(peer.getAddress(), tempPacket);

		// add the Message object to the transmit queue
		transmitQueue.push(msg);

		DEBUG("Transmit task added to the transmit queue. |MODE_CLIENT|");

		break;
	} // end of case MODE_CLIENT

	default:
	{
		WARN("Trying to ad a transmit task to the transmit queue. Client mode is not active.");
		break;
	} // end of default case
	} // end of switch(APP_MODE)

	//	transmitterMutex.unlock();
} // end of addTransmitTask()

/**
 * @brief This function waits until an entry in the transmit queue is done.
 * @details It checks only in the Client Mode any millisecond if an entry in the transmit queue is available.
 * 			If a Message is available there are two choices to send out the Message.
 * 			If the client uses the NTS service to encrypt data, the NTS service gets asked to create a request message
 * 			for sending out to the server. The NTS service then fill any ExtensionField with NTS content and sets the transmit Timestamp.
 * 			Finally it allows the transmit of the Message.<br>
 * 			If the client runs as a normal NTP-Client, the function sends out the first positioned Message of the transmit queue
 * 			after setting the transmit Timestamp.
 * 			The Message object will send out over the KernelInterface and the first entry of the transmit queue will clear.
 * 			After this, the other Threads get a chance to act while this Thread sleeps for 100 microseconds.
 */
void Transmitter::transmitProcess_Client(void)
{
	// break if the (fatal) exception pointer is set -> @see NTP.cpp or the program runs in server mode
	while (!(NTP::getExceptionPtr()) && NTP::getMode() == ntp::NTP::MODE_CLIENT)
	{
		// this transmit process do not do anything if the transmit queue is empty
		if (hasQueueMessage()) // is Thread-safe
		{
			DEBUG(std::string("Transmit task found. Getting to fill it."));

			//get the first positioned transmit task Message object and create the Interface to the NTS service
			ntp::Message xmitMsg = getMessageFromTransmitQueue(); //This is thread-safe

			bool sendMessage = false;

			if (USE_NTS)
			{
				try
				{
					NTPPacket & ntpPacket = xmitMsg.getPayload();
					std::string IpAddress = xmitMsg.getPeerAddress().toStringNoPort();
					std::vector<unsigned char> rawNtp = ntpPacket.createFrame();

					NtpPacketInfo pkifo;
					pkifo.setServer(IpAddress.c_str(), IpAddress.c_str(), 123);

					int ntpLen = rawNtp.size();
					int ntsContentSize = 0;
					bool status = true;


					// NTS-Nachricht erstellen (request): INIT
					ntsContentSize = ntp::NTP::getNtsUnicastService().createMessage_init(pkifo);
					if (ntsContentSize < 0)
					{
						printf("[FAIL] 'NTS_create_message_init()':  error code: %d\n", ntsContentSize);
						status = false;
					}
					else
					{
						// allocate space for NTS operations
						rawNtp.resize(ntpLen + ntsContentSize);
					}


					if (status == true)
					{
						// NTS-Nachricht erstellen (request): UPDATE
						int result = ntp::NTP::getNtsUnicastService().createMessage_update(pkifo, rawNtp.data(), ntpLen, rawNtp.size());
						if (result < 0)
						{
							printf("[FAIL] 'NTS_create_message_update()':  error code: %d\n", result);
							status = false;
						}
						else
						{
							ntpLen = result;
						}
					}

					if (status == true)
					{
						// NTP-Paket aus den Rohdaten erzeugen
						std::vector<uint8_t> rawTempNtpPacket = rawNtp;
						rawTempNtpPacket.resize(ntpLen);

						NTPPacket tempNtpPacket;
						tempNtpPacket.loadFromUDP(rawTempNtpPacket);

						// Transmit-Timestamp aktualisieren
						tempNtpPacket.setTransmitTime(ntp::KernelInterface::getInstance().getTime());

						// NTP-Paket wieder in Rohdaten wandeln
						rawTempNtpPacket = tempNtpPacket.createFrame();

						// den NTP-Header (Quelle) mit den aktualisieren Daten überschreiben
						for (int i = 0; i < 48; i++)
						{
							rawNtp[i] = rawTempNtpPacket[i];
						}

						// NTS-Nachricht erstellen (request): FINAL
						int result = ntp::NTP::getNtsUnicastService().createMessage_final(pkifo, rawNtp.data(), ntpLen, rawNtp.size(), false);
						if (result < 0)
						{
							printf("[FAIL] 'NTS_create_message_final()':  error code: %d\n", result);
							status = false;
						}
						else
						{
							ntpLen = result;
						}
					}

					if (status == true)
					{
						ntpPacket.loadFromUDP(rawNtp);
						sendMessage = true;
					}
					else
					{
						sendMessage = false;
					}

				}
				catch (std::string& e)
				{
					LOG_ERROR(std::string("NTS Exception in transmitpProcess_Client(): ") + e);
				}
				catch (std::exception& e)
				{
					LOG_ERROR(std::string("NTS Exception: ") + e.what());
				}
				catch (...)
				{
					LOG_ERROR("Unknown NTS  in transmitpProcess_Client().");
				}
			}    // end of if(useNTS)
			else // !useNTS
			{
				try
				{
					xmitMsg.getPayload().setTransmitTime(ntp::KernelInterface::getInstance().getTime());
				}
				catch (ntp::TimeException& ex)
				{
					LOG_ERROR("TimeException while setting the time in Transmitter::transmitProcess() : " + ex.toString());
				}
			} // end of else -> !useNTS

			 //start sending out if the NTS function has set the flag or the NTS service is inactive
			if (sendMessage == true || !USE_NTS)
			{
				try
				{
					// send out packet over the KernelInterface and log this event
					ntp::KernelInterface::getInstance().transmitPacket(xmitMsg);
					CHATTY_POLL_PROCESS(std::string("Deleted and send out the last send transmit Task."));
				} // end of try
				catch (ntp::NetworkException& ex)
				{
					WARN(std::string("Network Exception thrown while sending a packet in transmitProcess_Client()."));
					//ntp::NTP::setExceptionPtr(std::current_exception());
					if (ex.isFatal())
					{
						LOG_ERROR("Fatal Error in transmitProcess_Client(): " + ex.toString() + "\nExceptionPointer is set.");
						ntp::NTP::setExceptionPtr(std::current_exception());
					} // end of if(ex.isFatal())
				}     // end of catch
			}         // end of if(sendResponseMessage || !useNTS)
		}             // end of if transmitQueue.empty()

		//give the other threads a chance to handle.
		std::this_thread::yield();
		std::this_thread::sleep_for(std::chrono::milliseconds(250)); // to reduce processor load

	} // end of while
} //end of transmitProcess_Client()

/**
 * @brief This function is used to transmit a NTP Message in client mode immediately back to the requester.
 * @details To this, it creates a new request Message to send back to the server. This is necessary to identify this client
 * 			at the server and reversed. The transmitted Messages of the client are standard Messages only with
 * 			information about the used protocol version and the client mode. Therefore this function sets only this two values
 * 			and asks the NTS service to create a new NTS request Message to send out immediately.
 *
 * @see NTPPacket.hpp
 *
 * @param recvMsg The previously received Message object (from server).
*/
void ntp::Transmitter::fastTransmit_Client(ntp::Message recvMsg)
{
	ntp::KernelInterface& ki = ntp::KernelInterface::getInstance();

	// create send and receive packets
	ntp::NTPPacket sendPacket = ntp::NTPPacket();
	const ntp::NTPPacket& recvPacket = recvMsg.getPayload();

	// fill send packet with version number of the used protocol
	sendPacket.setVersion(recvPacket.getVersion());
	sendPacket.setMode(ntp::NTPMode::NTP_MODE_CLIENT);

	try
	{
		//create the message to send out
		ntp::Message sendMsg = ntp::Message(recvMsg.getPeerAddress(), sendPacket);

		NTPPacket & ntpPacket = sendMsg.getPayload();
		std::string IpAddress = sendMsg.getPeerAddress().toStringNoPort();
		std::vector<unsigned char> rawNtp = ntpPacket.createFrame();

		NtpPacketInfo pkifo;
		pkifo.setServer(IpAddress.c_str(), IpAddress.c_str(), 123);

		int ntpLen = rawNtp.size();
		int ntsContentSize = 0;
		bool status = true;
		bool sendMessage = false;

		// NTS-Nachricht erstellen (request): INIT
		ntsContentSize = ntp::NTP::getNtsUnicastService().createMessage_init(pkifo);
		if (ntsContentSize < 0)
		{
			printf("[FAIL] 'NTS_create_message_init()':  error code: %d\n", ntsContentSize);
			status = false;
		}
		else
		{
			// allocate space for NTS operations
			rawNtp.resize(ntpLen + ntsContentSize);
		}


		if (status == true)
		{
			// NTS-Nachricht erstellen (request): UPDATE
			int result = ntp::NTP::getNtsUnicastService().createMessage_update(pkifo, rawNtp.data(), ntpLen, rawNtp.size());
			if (result < 0)
			{
				printf("[FAIL] 'NTS_create_message_update()':  error code: %d\n", result);
				status = false;
			}
			else
			{
				ntpLen = result;
			}

		}

		if (status == true)
		{
			// NTP-Paket aus den Rohdaten erzeugen
			std::vector<uint8_t> rawTempNtpPacket = rawNtp;
			rawTempNtpPacket.resize(ntpLen);

			NTPPacket tempNtpPacket;
			tempNtpPacket.loadFromUDP(rawTempNtpPacket);

			// Transmit-Timestamp aktualisieren
			tempNtpPacket.setTransmitTime(ntp::KernelInterface::getInstance().getTime());

			// NTP-Paket wieder in Rohdaten wandeln
			rawTempNtpPacket = ntpPacket.createFrame();

			// den NTP-Header (Quelle) mit den aktualisieren Daten überschreiben
			for (int i = 0; i < 48; i++)
			{
				rawNtp[i] = rawTempNtpPacket[i];
			}



			// NTS-Nachricht erstellen (request): FINAL
			int result = ntp::NTP::getNtsUnicastService().createMessage_final(pkifo, rawNtp.data(), ntpLen, rawNtp.size(), false);
			if (result < 0)
			{
				printf("[FAIL] 'NTS_create_message_final()':  error code: %d\n", result);
			}
			else
			{
				ntpLen = result;
				status = false;
			}
		}

		if (status == true)
		{
			ntpPacket.loadFromUDP(rawNtp);
			sendMessage = true;
		}
		else
		{
			sendMessage = false;
		}


		// Sending, if NTS checked the packet
		if (sendMessage == true)
		{
			ki.transmitPacket(sendMsg);

			// log important info messages
			if (NTP::getMode() == NTP::AppMode::MODE_SERVER)
				DEBUG("Send back the received Packet to the Client in Transmitter::fastTransmit().");

			if (NTP::getMode() == NTP::AppMode::MODE_CLIENT)
				DEBUG("Send back a received Packet to the Server in Transmitter::fastTransmit().");
		}
		else
			DEBUG("Could not send out packet in fastTransmit(), NTS do not allow this.");
	} //end of try
	catch (ntp::NetworkException& ex)
	{
		WARN("Network Exception thrown while sending a packet in Transmitter::fastTransmit().");
		if (ex.isFatal())
		{
			LOG_ERROR("Fatal Error in Transmitter::fastTransmit():" + ex.toString() + "\nExceptionPointer is set.");
			ntp::NTP::setExceptionPtr(std::current_exception());
		}
		return;
	} // end of catch(NetworkException)
	catch (std::string& e)
	{
		LOG_ERROR(std::string("NTS Exception in Transmitter::fastTransmit(): ") + e);
		return;
	}
	catch (...)
	{
		LOG_ERROR("Unknown NTS Exception in Transmitter::fastTransmit()!");
		return;
	}
}

/**
 * @brief This function is used to transmit a NTP Message in server mode immediately back to the requester.
 * @details If the previously received packet (given to this function as a parameter) contents an extensionField and is a secure packet
 * 			with NTS messages, it validates the packet with the function processingNTPMessage() of the NTS-Service. If the received
 * 			packet contents no extension field or no NTS data, it is interpreted as a normal NTP request.
 *
 * 			In both cases this function fill some data before it send the message back to the client:
 * 			<ul>
 * 			<li>Protocol Version (from the received message)</li>
 * 			<li>Mode of the Host</li>
 * 			<li>Source Address (from the received message)</li>
 * 			<li>Destination Address (from the received message)</li>
 * 			<li>Leap Indicator</li>
 * 			<li>Stratum</li>
 * 			<li>Poll Interval (from the received message)</li>
 * 			<li>System Clock Precision</li>
 * 			<li>Root Delay</li>
 * 			<li>Root Dispersion</li>
 * 			<li>Reference ID</li>
 * 			<li>Reference Time</li>
 * 			<li>Origin Timestamp (= transmit Timestamp of the received message)</li>
 * 			<li>Receive Timestamp (= destination Timestamp of the received message)</li>
 * 			<li>Transmit Timestamp (actual time of the reference clock) -> This will only set by this function if NTS is not active.</li>
 * 			</ul>
 *
 * @see NTPPacket.hpp
 * @see NTPPacket.cpp
 *
 * @param recvMsg The previously received Message object (from client).
 * @remarks How to set the leap-indicator as reference-clock?
 *
 */
void ntp::Transmitter::fastTransmit_Server(ntp::Message recvMsg)
{
	ntp::KernelInterface& ki = ntp::KernelInterface::getInstance();

	// create packets for sending out and for the received message
	ntp::NTPPacket sendPacket = ntp::NTPPacket();
	const ntp::NTPPacket& recvPacket = recvMsg.getPayload();

	// get the extension field list to evaluate if the received message is a NTS message
	std::list<ntp::ExtensionField>& extFields = recvMsg.getPayload().getExtensionFields();
	bool useNTS = false;

	//check if there is any extension field in the received message
	if (extFields.size() > 0)
	{
		ntp::ExtensionField extField = extFields.front();

		// NTS should used if the extension field value is equal to NTSFieldType given in the config.ini
		//		if(extField.getFieldType() == static_cast<unsigned short int>(NTS_FIELD_TYPE))
		useNTS = true;
	}

	// fill send packet with version number of the used protocol and the used mode
	sendPacket.setVersion(recvPacket.getVersion());
	sendPacket.setMode(ntp::NTPMode::NTP_MODE_SERVER);

	sendPacket.setPrecision(ki.getSystemPrecision());
	sendPacket.setOriginTime(recvPacket.getTransmitTime());
	sendPacket.setReceiveTime(recvMsg.getDestTimestamp()); //Destination Timestamp is the time, when the packet arrives at the server.

	// fill the send packet with important data
	// get Some data from the client process out of the shared memory
	SharedSystemVariables systemVariables = ki.getSystemVariables();

	// We are the reference clock
	if (REFERENCE_CLOCK)
	{
		//For now, the LeapIndicator will always be set to synchronized.
		sendPacket.setLeapIndicator(LeapIndicator::LI_NO_WARNING);
		sendPacket.setStratum(1);
		sendPacket.setReferenceId(REFERENCE_ID);
		sendPacket.setPoll(static_cast<signed char>(POLL));
		sendPacket.setRootdelay(0);
		sendPacket.setRootdispersion(0);
		sendPacket.setReferenceTime(ki.getTime());
	}
	else // in this case we are a peer with stratum > 2
	{
		sendPacket.setStratum(systemVariables.getStratum());
		sendPacket.setReferenceId(systemVariables.getReferenceID().c_str());
		sendPacket.setPoll(systemVariables.getPoll());
		sendPacket.setRootdelay(systemVariables.getRootDelay());
		sendPacket.setRootdispersion(systemVariables.getRootDispersion());
		sendPacket.setReferenceTime(systemVariables.getReferenceTimestamp());
		try
		{
			sendPacket.setLeapIndicator(ki.getLeapSecond());
		}
		catch (ntp::TimeException& ex)
		{
			LOG_ERROR("TimeException while reading the LeapIndicator in Transmitter::fastTransmit() : " + ex.toString());
		}
	}

	// this Timestamp will automatically be updated by the NTS service, if it is active
	sendPacket.setTransmitTime(ki.getTime());

	try
	{
		//create the message to send out
		ntp::Message sendMsg = ntp::Message(recvMsg.getPeerAddress(), sendPacket);

		// NTS validation and creating of a response packet only, if NTS is activated
		if (useNTS)
		{

			std::vector<unsigned char> sendMsgRaw = sendMsg.getPayload().createFrame();
			std::vector<unsigned char> recvMsgRaw = recvMsg.getPayload().createFrame();
			std::string IpAddress = sendMsg.getPeerAddress().toStringNoPort();
			
			NtpPacketInfo pkifo;
			pkifo.setServer(IpAddress.c_str(), IpAddress.c_str(), 123);

			int ntpLen = recvMsgRaw.size();
			int ntsContentSize = 0;
			bool status = true;
			bool sendMessage = false;
			int ntsSpaceAvailable = 0;




			// NTS-Nachricht überprüfen und parsen
			int result = ntp::NTP::getNtsUnicastService().processAndVerify(pkifo, recvMsgRaw.data(), ntpLen, recvMsgRaw.size());

			if (result < 0)
			{
				printf("[FAIL] 'NTS_process_and_verify()':  error code: %d\n", result);
				status = false;
			}
			else
			{
				printf("[SUCCESS] 'NTS_process_and_verify()': current NTS content size: %d bytes\n", result);
				ntsSpaceAvailable = result;
			}



			if (status == true)
			{
				// NTS-Nachricht erstellen (request): INIT
				ntsContentSize = ntp::NTP::getNtsUnicastService().createMessage_init(pkifo);
				if (ntsContentSize < 0)
				{
					printf("[FAIL] 'NTS_create_message_init()':  error code: %d\n", ntsContentSize);
					status = false;
				}
				else
				{
					if ((ntsContentSize - ntsSpaceAvailable) > 0)
					{
						// allocate space for NTS operations
						recvMsgRaw.resize(ntpLen + (ntsContentSize - ntsSpaceAvailable));
					}
				}
			}



			if (status == true)
			{
				// NTS-Nachricht erstellen (request): UPDATE
				int result = ntp::NTP::getNtsUnicastService().createMessage_update(pkifo, recvMsgRaw.data(), ntpLen, recvMsgRaw.size());
				if (result < 0)
				{
					printf("[FAIL] 'NTS_create_message_update()':  error code: %d\n", result);
					status = false;
				}
				else
				{
					ntpLen = result;
				}

			}

			if (status == true)
			{

				// den NTP-Header (Request) mit den header-Daten des Antwort-NTP-Paktes überschreiben
				for (int i = 0; i < 48; i++)
				{
					recvMsgRaw[i] = sendMsgRaw[i];
				}

				// NTP-Paket aus den Rohdaten erzeugen
				std::vector<uint8_t> rawTempNtpPacket = recvMsgRaw;
				rawTempNtpPacket.resize(ntpLen);
				NTPPacket tempNtpPacketRec;
				tempNtpPacketRec.loadFromUDP(rawTempNtpPacket);

				// Transmit-Timestamp aktualisieren
				tempNtpPacketRec.setTransmitTime(ntp::KernelInterface::getInstance().getTime());

				// NTP-Paket wieder in Rohdaten wandeln
				rawTempNtpPacket = tempNtpPacketRec.createFrame();


				// den NTP-Header (Quelle) mit den aktualisieren Daten überschreiben
				for (int i = 0; i < 48; i++)
				{
					recvMsgRaw[i] = rawTempNtpPacket[i];
				}


				// NTS-Nachricht erstellen (request): FINAL
				int result = ntp::NTP::getNtsUnicastService().createMessage_final(pkifo, recvMsgRaw.data(), ntpLen, recvMsgRaw.size(), false);
				if (result < 0)
				{
					printf("[FAIL] 'NTS_create_message_final()':  error code: %d\n", result);
					status = false;
				}
				else
				{
					ntpLen = result;
				}
			}

			if (status == true)
			{
				sendMsg.getPayload().loadFromUDP(recvMsgRaw);
				sendMessage = true;
			}
			else
			{
				sendMessage = false;
			}


			 //Sending, if NTS checked the packet
			if (sendMessage == true)
			{
				ki.transmitPacket(sendMsg);
				

				// log important info messages
				if (NTP::getMode() == NTP::AppMode::MODE_SERVER)
					DEBUG("Send back the received Packet to the Client in Transmitter::fastTransmit().");

				if (NTP::getMode() == NTP::AppMode::MODE_CLIENT)
					DEBUG("Send back a received Packet to the Server in Transmitter::fastTransmit().");
			}
			else
				DEBUG("Could not send out packet in fastTransmit(), NTS do not allow this.");


		}    // end of if(useNTS)
		else // case !useNTS
		{
			// NTS is not active -> send out the response packet in server mode
			ki.transmitPacket(sendMsg);
			DEBUG("Send back the received Packet to the Client in Transmitter::fastTransmit(). [without NTS]");
		} //end of else -> !useNTS
	}     //end of try
	catch (ntp::NetworkException& ex)
	{
		WARN("Network Exception thrown while sending a packet in Transmitter::fastTransmit().");
		if (ex.isFatal())
		{
			LOG_ERROR("Fatal Error in Transmitter::fastTransmit():" + ex.toString() + "\nExceptionPointer is set.");
			ntp::NTP::setExceptionPtr(std::current_exception());
		}
		return;
	} // end of catch(NetworkException)
	catch (std::string& e)
	{
		LOG_ERROR(std::string("NTS Exception in Transmitter::fastTransmit(): ") + e);
		return;
	}
	catch (...)
	{
		LOG_ERROR("Unknown NTS Exception in Transmitter::fastTransmit()!");
		return;
	}
}

/**
 * @brief This function is to check whether the transmit queue has an entry.
 * @details It is a thread-safe function which is used in the transmit_Client process.
 *
 * @return It return a boolean value. This is TRUE if the transmit queue has one or more entries and FALSE if not.
 */
bool ntp::Transmitter::hasQueueMessage()
{
	// if the program quits or an uncatched exception occurs, the lock guard ensures the unlocking of the mutex.
	std::lock_guard<std::mutex> lg(this->transmitterMutex);

	//returns whether the transmit queue is empty or not.
	return this->transmitQueue.size() > 0;
}

/**
 * @brief This getter-function is to pop the first element of the transmit queue.
 * @details It is a thread-safe function which is used in the transmit_Client process to get the Message to send out.
 *
 * @return It returns the first Message object of the queue.
 */
ntp::Message ntp::Transmitter::getMessageFromTransmitQueue()
{
	// if the program quits or an uncatched exception occurs, the lock guard ensures the unlocking of the mutex.
	std::lock_guard<std::mutex> lg(this->transmitterMutex);

	// dummy return value
	ntp::Message ret = ntp::Message(IPAddress(), ntp::NTPPacket());

	//if the queue is empty, return dummy return value
	if (this->transmitQueue.size() == 0)
	{
		return ret;
	}
	else
	{
		//get the first positioned transmit task Message object and create the Interface to the NTS service
		ret = this->transmitQueue.front();

		// delete the first task of the queue. This task was processed.
		transmitQueue.pop();

		return ret;
	}
}

/**
 * @brief This function will call, if a functor object of this class will give to a thread constructor.
 * @details If this Thread starts, the transmitProcess_Client() function will call by this operator()-function in <strong>Client Mode</Strong>.
 * 			in <strong>Server Mode</strong> it logs only an error, because no transmit process is needed in this mode.
 * 			The start of the respective function will log as an info event in Client mode and as an error event in other modes.
 *
 * @see transmitProcess()
 */
void ntp::Transmitter::operator()(void)
{
	switch (NTP::getMode())
	{
	case ntp::NTP::MODE_CLIENT:
		DEBUG("Operator() of Transmitter called in Client Mode, TransmitProcess_Client started.");
		transmitProcess_Client();
		break;

	case ntp::NTP::MODE_SERVER:
		LOG_ERROR("Operator() of Transmitter called in Server Mode, no transmit process started.");
		break;

	default:
		LOG_ERROR("Operator() of Transmitter called in unknown Mode, no transmit process started.");
	}
}

} //end of namespace ntp
