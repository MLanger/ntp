/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	NTP.hpp
 * @details This Header declares the NTP Class, which is the entrance point to the program
 * @date 	11.05.2016
 * @author 	Christian Jütte
 * @version	0.1
 */

#ifndef NTP_NTP_HPP_
#define NTP_NTP_HPP_

#include "Utility/ConfigFile.hpp"
#include <NtsUnicastService.h>
#include <exception>

#define STR_EXPAND(token) #token
#define STR(token) STR_EXPAND(token)

#define NTP_MAJOR_VERSION 0
#define NTP_MINOR_VERSION 6
#define NTP_PATCH_VERSION 0
constexpr char NTP_COMPILE_TIME[] = __DATE__ " @ " __TIME__;
constexpr char NTP_VERSION[] = STR(NTP_MAJOR_VERSION) "." STR(NTP_MINOR_VERSION) "." STR(NTP_PATCH_VERSION);

namespace ntp
{

/**
 * @brief Class NTP - Entrance to the NTP-application.
 * @details This class initializes and starts the threads depending on the mode of the application.
 * 			It also stores the mode (Server, Client) of the application.
 */
class NTP
{
public:
	/// Constants to define the Application-Mode
	enum AppMode
	{
		///Value for Server-Application-Mode
		MODE_SERVER = 0,
		///Value for Client-Application-Mode
		MODE_CLIENT = 1
	};

private:
	///Contains the current application-mode (loaded from config) (default = Server)
	static AppMode mode;
	///exception pointer to handle exceptions from running threads.
	static std::exception_ptr exceptionPtr;
	/// the NtsInstance
	static NtsUnicastService nts;

	void setAppModeFromConfig(ConfigFile& config);
	void setLoggingModes(ConfigFile& config);
	void loadLoggingMode(ConfigFile& config);
	void setLoggingDir(ConfigFile& config);

public:
	NTP();
	~NTP();
	static AppMode getMode();
	static void setExceptionPtr(std::exception_ptr exceptionPtr);
	static std::exception_ptr getExceptionPtr();
	static NtsUnicastService& getNtsUnicastService();
}; //end of class NTP

} //end of namespace ntp

#endif /* NTP_NTP_HPP_ */
