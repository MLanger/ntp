/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file ExtensionField.hpp
 *
 * @date	31.01.2017
 * @author	silvan
 * @version	0.1
 */

#ifndef NTP_DATATYPES_FRAME_EXTENSIONFIELD_HPP_
#define NTP_DATATYPES_FRAME_EXTENSIONFIELD_HPP_

#include <iostream>
#include <string.h>
#include <vector>

/// minimum length of the entire extension field
#define EXTENSION_FIELD_MINIMUM_LENGTH 16
/// length of the header (type field + length field)
#define EXTENSION_FIELD_HEADER_LENGTH 4
/// minimum length of the value field
#define EXTENSION_FIELD_MIN_VALUE_LENGTH (EXTENSION_FIELD_MINIMUM_LENGTH - EXTENSION_FIELD_HEADER_LENGTH)

// minimal size of an extension field in octets
//#define MIN_EXTENSIONFIELD_SIZE 16

namespace ntp
{

/**
 * @brief 	this class represents an extension field
 * @details	This class represents an extension field as defined in RFC5905.
 * 			It stores the field type and the value of the extension field. It
 * 			automatically calculates the length of the entire field and also generates
 * 			padding bytes if necessary.
 */
class ExtensionField
{
private:
	unsigned short fieldType;
	std::vector<unsigned char> value;

public:
	ExtensionField();
	ExtensionField(unsigned short fieldType, const std::vector<unsigned char>& value);
	~ExtensionField();

	void setValue(const std::vector<unsigned char>& value);
	void setFieldType(unsigned short fieldType);
	std::vector<unsigned char> getValue() const;
	unsigned short getFieldType() const;
	unsigned short getLength() const;

	void loadFromRaw(std::vector<unsigned char>::iterator& start,
	                 const std::vector<unsigned char>::iterator& max);

	std::vector<unsigned char> getRaw() const;
	std::string toString() const;
};

} // end of namespace ntp

#endif /* NTP_DATATYPES_FRAME_EXTENSIONFIELD_HPP_ */
