/**
 * @file ExtensionField.cpp
 *
 * @date	31.01.2017
 * @author	silvan
 * @version	0.1
 */

#include "ExtensionField.hpp"
#include "../../Log/Logger.hpp"
#include "../Exceptions/InvalidPacketException.hpp"
#include <sstream>

namespace ntp
{

/**
 * 	@brief	standard constructor
 */
ntp::ExtensionField::ExtensionField() : value()
{
	this->fieldType = 0;
}

/**
 * @brief		constructor, initializing the variables with the values
 * 				of the variables passed to the constructor
 */
ntp::ExtensionField::ExtensionField(unsigned short fieldType, const std::vector<unsigned char>& value)
{
	this->fieldType = fieldType;
	this->setValue(value);
}

ntp::ExtensionField::~ExtensionField()
{
}

/**
 * @brief      	sets the vector "value"
 * @details		This method will create a copy of value and adds padding bytes
 *				to the copy automatically if necessary. So, if you try to get
 *				the value using ExtensionField::getValue, you will always get
 *				the zero padded value field.
 */
void ntp::ExtensionField::setValue(const std::vector<unsigned char>& value)
{
	this->value = value;
	unsigned short length = this->value.size();
	unsigned short padding = 0;

	// determine necessary padding bytes
	if (length < EXTENSION_FIELD_MIN_VALUE_LENGTH)
	{
		// add padding to match minimum extension field length criteria
		padding = EXTENSION_FIELD_MIN_VALUE_LENGTH - length;
	}
	else
	{
		padding = (length % 4) == 0 ? 0 : (4 - (length % 4));
	}

	// fill padding bytes
	for (; padding > 0; padding--)
	{
		this->value.push_back(0);
	}
}

/**
 * @brief      	sets the fieldType of the ExtensionField
 *
 * @param[in]  	fieldType          	unsigned short
 *
 * @return    	void
 */
void ntp::ExtensionField::setFieldType(unsigned short fieldType)
{
	this->fieldType = fieldType;
}

/**
 * @brief      	returns the zero padded value of the ExtensionField
 */
std::vector<unsigned char> ntp::ExtensionField::getValue() const
{
	return this->value;
}

/**
 * @brief      	returns the fieldType of the ExtensionField
 */
unsigned short ntp::ExtensionField::getFieldType() const
{
	return this->fieldType;
}

/**
 * @brief      	returns the length of the ExtensionField
 * @details		The returned value is the length of the entire extension field
 * 				including header and padding bytes.
 */
unsigned short ntp::ExtensionField::getLength() const
{
	register unsigned short length = 0;
	length += EXTENSION_FIELD_HEADER_LENGTH;
	length += this->value.size(); // already zero padded

	return length;
}

/**
 * @brief	loads the extension field from a raw byte vector
 * @details	Takes an iterator to a byte vector. The iterator will be incremented during
 * 			execution of this method. When this method is left, the iterator was incremented
 * 			by the length of the parsed ExtensionField. Doing so, you are able to parse
 * 			several ExtensionFields one by one using the same iterator as the following
 * 			example shows:
 * @code
std::vector<unsigned char>::iterator cursor = vek.begin();
while( (vek.end()-cursor) >= EXTENSION_FIELD_MINIMUM_LENGTH)
{
   ExtensionField ex;
   ex.loadFromRaw(cursor, vek.end());
}
 * @endcode
 *
 *			Please note, that you should manually check if an extension field has got the
 *			the least possible length before decoding it.
 * 			However, if the length field of the decoded ExtensionField announces a length
 * 			which is more than the available bytes, decoding might lead to errors. To avoid
 * 			this, the second parameter will tell this method if more bytes are available.
 * 			If the length exceeds the given maximum, this method throws an InvalidPacketException
 * 			of type InvalidPacketException::EXTENSION_FIELD_WRONG_LENGTH. The parameter start
 * 			will become incremented independent of the occurrence of the exception. So after
 * 			the exception was thrown, start is equal to max.
 *
 * 			This method will throw an InvalidPacketException of type
 * 			InvalidPacketException::EXTENSION_FIELD_TOO_SHORT if either
 * 			the decoded length of the extension field is too short or if
 * 			the zero padding is missing.
 */
void ExtensionField::loadFromRaw(std::vector<unsigned char>::iterator& start,
                                 const std::vector<unsigned char>::iterator& max)
{

	this->fieldType = (*start) << 8;
	start++;
	this->fieldType |= *start;
	start++;

	register unsigned short length = *start << 8;
	start++;
	length |= *start;
	start++;

	if (length < EXTENSION_FIELD_MINIMUM_LENGTH)
	{
		throw InvalidPacketException("wrong length in ExtensionField: too short",
		                             InvalidPacketException::EXTENSION_FIELD_TOO_SHORT);
	}
	if (length % 4)
	{
		throw InvalidPacketException("length in ExtensionField: no zero padding",
		                             InvalidPacketException::EXTENSION_FIELD_TOO_SHORT);
	}

	length -= EXTENSION_FIELD_HEADER_LENGTH;

	this->value.clear();

	if (start + length <= max)
	{
		this->value.insert(this->value.end(), start, start + length);
		start += length;
	}
	else
	{
		this->value.insert(this->value.end(), start, max);
		start = max;
		throw InvalidPacketException("ExtensionField is shorter than announced by its length field",
		                             InvalidPacketException::EXTENSION_FIELD_WRONG_LENGTH);
	}
}

/**
 * @brief      	generates a data stream of the ExtensionField
 * @details		For information about the format see RFC5905.
 */
std::vector<unsigned char> ntp::ExtensionField::getRaw() const
{
	std::vector<unsigned char> raw;

	unsigned short fieldType;
	fieldType = this->getFieldType();

	raw.push_back(static_cast<unsigned char>(fieldType >> 8));     // get first byte
	raw.push_back(static_cast<unsigned char>(0x00FF & fieldType)); // get second byte

	unsigned short const length = this->getLength();

	raw.push_back(static_cast<unsigned char>(length >> 8));     // get first byte
	raw.push_back(static_cast<unsigned char>(0x00FF & length)); // get second byte

	raw.insert(raw.end(), this->value.begin(), this->value.end());

	return raw;
}

/**
 * @brief      	generates a string consisting the class variables
 * @details		The string has the following format: "Type: <type>; Length: <length>".
 */
std::string ntp::ExtensionField::toString() const
{
	std::stringstream builder;

	builder << "Type: " << this->fieldType << "; Length: " << this->getLength();

	return builder.str();
}

} // end of namespace ntp
