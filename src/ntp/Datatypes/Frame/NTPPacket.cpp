/**
 * @file NTPPacket.cpp
 *
 * @date	25.03.2016
 * @author	silvan
 * @version	0.1
 */

#include "NTPPacket.hpp"

#include "../../Utility/Constants.hpp"
#include "../Exceptions/InvalidPacketException.hpp"
#include "../IPAddress.hpp"
#include <cstdio>
#include <iostream>
#include <string>

namespace ntp
{

using namespace std;

/// does only initialize fields
NTPPacket::NTPPacket(void)
{
	leapIndicator = LI_UNKNOWN;
	version = 0;
	mode = NTP_MODE_RESERVED;
	stratum = 0;
	poll = 0;
	precision = 0;
	extensionFields = list<ExtensionField>();
	referenceID[0] = 0;
	referenceID[1] = 0;
	referenceID[2] = 0;
	referenceID[3] = 0;
	referenceID[4] = 0;
}

/// does not do anything
NTPPacket::~NTPPacket(void)
{
	// extension fields destroyed?
}

/// returns string-representation of this packet
/**
 * @details		This function is intended to be used for easy debugging. It returns
 * 				a string (without newlines) in the format:
 *
 * @code
Version=<version>, Leap Indicator=<leap_indicator>, Mode=<mode>, Stratum=<stratum>,
Poll=<poll>, Precision=<precision>, root delay=<rootDelay>, root dispersion=<rootDispersion>,
referenceID=<referenceID>, originTime=<orgtime>, recvTime=<recvtime>, transmitTime=<transtime>,
refTime=<reftime>"
 * @endcode
 *
 * @return	string
 */
string NTPPacket::toString() const
{

#define HERE DEBUG("HERE");

	string
	    s = "",
	    version = "",
	    stratum = "",
	    poll = "",
	    precision = "",
	    rootDelay = "",
	    rootDispersion = "",
	    referenceID = "",
	    orgtime = "",
	    recvtime = "",
	    transtime = "",
	    reftime = "",
	    ext_fields = "",
	    mode = "",
	    leap_indicator = "";

	char buffer[255];
	s.reserve(200); //allocates memory for 200 characters.

	// leap indicator to string
	sprintf(buffer, "%u", (unsigned int)this->leapIndicator);
	leap_indicator = buffer;

	// version to string
	sprintf(buffer, "%u", (unsigned int)this->version);
	version = buffer;

	// mode to string
	sprintf(buffer, "%u", (unsigned int)this->mode);
	mode = buffer;

	// stratum to string
	sprintf(buffer, "%u", (unsigned int)this->stratum);
	stratum = buffer;

	// poll to string
	sprintf(buffer, "%i", (int)this->poll);
	poll = buffer;

	// precision to string
	sprintf(buffer, "%i", (int)this->precision);
	precision = buffer;

	// rootDelay (ShortTimestamp) to string
	sprintf(buffer, "%.4f", static_cast<double>(this->rootDelay));
	rootDelay = buffer;

	// rootDispersion (ShortTimestamp) to string
	sprintf(buffer, "%.4f", static_cast<double>(this->rootDispersion));
	rootDispersion = buffer;

	// reference id to string (char array)
	referenceID = refIDtoString();
	orgtime = originTimestamp.toString();
	recvtime = receiveTimestamp.toString();
	transtime = transmitTimestamp.toString();
	reftime = referenceTimestamp.toString();

	string extFields = "";
	int counter = 1;
	for (auto field : this->extensionFields)
	{
		extFields += string("ExtensionField#") + to_string(counter) + ": " + field.toString() + " ** ";
		counter++;
	}
	//std::cout << this << std::endl;
	// build string
	s = string("Version=") + version + string(", Leap Indicator=") + leap_indicator +
	    string(", Mode=") + mode + string(", Stratum=") + stratum + string(", Poll=") + poll +
	    string(", Precision=") + precision + string(", root delay=") + rootDelay +
	    string(", root dispersion=") + rootDispersion + string(", referenceID=") + referenceID +
	    string(", originTime=") + orgtime + string(", recvTime=") + recvtime +
	    string(", transmitTime=") + transtime + string(", refTime=") + reftime +
	    " | " + extFields;

	return s;
}

/**
 * @brief	interprets referenceID depending on the stratum
 * @details	Returns "[KissCode]" if stratum is 0. Returns the referenceID interpreted as string if
 * 			stratum is 1. For any different stratum, it returns "[IPAddr]".
 */
std::string NTPPacket::refIDtoString() const
{
	if (this->stratum == 0 || this->stratum == 1)
	{
		return std::string(this->referenceID);
	}
	else
	{
		const uint32_t* address = reinterpret_cast<const uint32_t*>(this->referenceID);
		IPAddress addr(*address, 0);
		return addr.toStringNoPort();
	}
}

/// returns an unsigned char vector which can be written to a socket
/**
 * @remarks	This seems to produce a frame in a correct byte order. However, it has not been checked on
 * 			machines which are not little-endian. We should do it some day.
 *
 * @return	bytes of the frame
 */
vector<unsigned char> NTPPacket::createFrame()
{

	vector<unsigned char> frame(0);
	vector<unsigned char> tmp;

	//ORGINAL:    frame.reserve(MIN_FRAME_SIZE * 2); // double minimum size should be enough to start with
	frame.reserve(1500); // MTU size (sollte f�r NTS reichen ;)

	frame.push_back((leapIndicator << 6) | ((version & 0x07) << 3) | ((mode & 0x07) << 0));
	frame.push_back(stratum);
	frame.push_back(reinterpret_cast<unsigned char&>(poll));
	frame.push_back(reinterpret_cast<unsigned char&>(precision));

	tmp = rootDelay.getRaw();
	frame.insert(frame.end(), tmp.begin(), tmp.end());

	tmp = rootDispersion.getRaw();
	frame.insert(frame.end(), tmp.begin(), tmp.end());

	frame.insert(frame.end(), this->referenceID, this->referenceID + 4);

	tmp = referenceTimestamp.getRaw();
	frame.insert(frame.end(), tmp.begin(), tmp.end());

	tmp = originTimestamp.getRaw();
	frame.insert(frame.end(), tmp.begin(), tmp.end());

	tmp = receiveTimestamp.getRaw();
	frame.insert(frame.end(), tmp.begin(), tmp.end());

	tmp = transmitTimestamp.getRaw();
	frame.insert(frame.end(), tmp.begin(), tmp.end());

	for (ExtensionField e : this->extensionFields)
	{
		auto raw = e.getRaw();

		frame.insert(frame.end(), raw.begin(), raw.end());
	}

	return frame;
}

/// decodes an NTP packet
/*
 * @details		This method is used to decode a NTP packet and fill this
 * 				objects fields with the data given. This method will throw
 * 				an InvalidPacketException when the packet does not match the
 * 				requirements (e.g. too short).
 */
void NTPPacket::loadFromUDP(std::vector<unsigned char> & data)
{

	if (data.size() >= MIN_FRAME_SIZE && !(data.size() % 4)) // minimum size and correct padding
	{
		DEBUG("Header might be valid");
	}
	else
	{
		//throw an exception
		throw(InvalidPacketException("Packet is too short(" + std::to_string(data.size()) + " bytes) or padding incorrect!",
		                             InvalidPacketException::PACKET_TOO_SHORT));
	}

	std::vector<unsigned char>::iterator cursor = data.begin();

	this->leapIndicator = *cursor >> 6;
	this->version = (*cursor & 0x38) >> 3;
	this->mode = *cursor & 0x07;
	cursor++;
	this->stratum = *cursor;
	cursor++;
	this->poll = reinterpret_cast<signed char&>(*cursor);
	cursor++;
	this->precision = reinterpret_cast<signed char&>(*cursor);
	cursor++;

	this->rootDelay.loadFromRaw(cursor);
	this->rootDispersion.loadFromRaw(cursor);

	for (int i = 0; i <= 3; i++)
	{
		this->referenceID[i] = *cursor;
		cursor++;
	}

	this->referenceTimestamp.loadFromRaw(cursor);
	this->originTimestamp.loadFromRaw(cursor);
	this->receiveTimestamp.loadFromRaw(cursor);
	this->transmitTimestamp.loadFromRaw(cursor);

	// decode extension fields
	//	DEBUG(string("number of bytes before decoding: ")+to_string(static_cast<int>(data.end()-cursor)));
	this->extensionFields.clear();
	while ((data.end() - cursor) >= EXTENSION_FIELD_MINIMUM_LENGTH)
	{
		ExtensionField field;
		field.loadFromRaw(cursor, data.end());
		this->extensionFields.push_back(field);
	}
	//	DEBUG(string("number of bytes after decoding extension fields: ")
	//			+std::to_string(static_cast<int>(data.end()-cursor)) );
}

//##########################################################################
//################## set/get methods #######################################
//##########################################################################

/// returns a list of extension fields
list<ExtensionField>& NTPPacket::getExtensionFields()
{
	return this->extensionFields;
}

/// adds an extension field
void NTPPacket::addExtensionField(ExtensionField field)
{
	this->extensionFields.push_back(field);
}

/// sets the leap indicator
/**
 * For allowed values, see LeapIndicator.
 * @param[in] l leap indicator
 */
void NTPPacket::setLeapIndicator(LeapIndicator l)
{
	this->leapIndicator = l;
}

/// returns the leap indicator
/**
 * For possible values, see LeapIndicator.
 * @return	leap indicator
 */
LeapIndicator NTPPacket::getLeapIndicator() const
{
	return (LeapIndicator)this->leapIndicator;
}

/// sets the protocol version
/**
 * Only values <=7 are allowed. Higher values will default to 0.
 * @param[in]	version
 */
void NTPPacket::setVersion(unsigned int version)
{
	if (version > 7)
	{
		version = 0;
	}
	this->version = version;
}

/// returns the protocol version
/**
 * Only values <=7 are allowed. Higher values will default to 0.
 * @return version
 */
unsigned int NTPPacket::getVersion() const
{
	return this->version;
}

/// sets the protocol mode
/**
 * For allowed values, see NTPPacket::NTPMode.
 * @param[in]	m	modus
 */
void NTPPacket::setMode(NTPMode m)
{
	this->mode = m;
}

/// returns the protocol mode
/**
 * For allowed values, see NTPMode.
 * @return modus
 */
NTPMode NTPPacket::getMode() const
{
	return (NTPMode)this->mode;
}

/// returns the origin timestamp
LongTimestamp NTPPacket::getOriginTime() const
{
	return this->originTimestamp;
}

/// sets the origin timestamp
void NTPPacket::setOriginTime(LongTimestamp originTime)
{
	this->originTimestamp = originTime;
}

/// returns the poll interval
signed char NTPPacket::getPoll() const
{
	return this->poll;
}

/// sets the poll interval
/**
 * @details If the given poll interval is greater than POLL_MAX or
 * 			less than POLL_MIN, it will default to POLL_MAX.
 * 			(just as suggested in RFC5905 page 21)
 * 			Those constants will be loaded from class Constants.
 * @param[in]	poll	poll intervall
 */
void NTPPacket::setPoll(signed char poll)
{
	Constants& C = Constants::getInstance();
	const register unsigned char POLL_MAX = C.getUChar(ConstantsUChar::pollMax);
	const register unsigned char POLL_MIN = C.getUChar(ConstantsUChar::pollMin);
	if ((poll > POLL_MAX) || (poll < POLL_MIN))
	{
		poll = POLL_MAX;
	}
	this->poll = poll;
}

/// returns the precision
signed char NTPPacket::getPrecision() const
{
	return this->precision;
}

/// sets the precision
void NTPPacket::setPrecision(signed char precision)
{
	this->precision = precision;
}

/// gets the receive timestamp
LongTimestamp NTPPacket::getReceiveTime() const
{
	return this->receiveTimestamp;
}

/// sets the receive timestamp
void NTPPacket::setReceiveTime(LongTimestamp receiveTime)
{
	this->receiveTimestamp = receiveTime;
}

/// returns the reference ID
string NTPPacket::getReferenceId() const
{
	return string(referenceID);
}

/// sets the reference ID
void NTPPacket::setReferenceId(char const* referenceID)
{
	for (int i = 0; i < 4; i++)
	{
		this->referenceID[i] = referenceID[i];
	}
}

/// returns the reference timestamp
LongTimestamp NTPPacket::getReferenceTime() const
{
	return this->referenceTimestamp;
}

/// sets the reference timestamp
void NTPPacket::setReferenceTime(LongTimestamp referenceTime)
{
	this->referenceTimestamp = referenceTime;
}

/// returns root delay
ShortTimestamp NTPPacket::getRootdelay() const
{
	return this->rootDelay;
}

/// sets the root delay
void NTPPacket::setRootdelay(ShortTimestamp rootDelay)
{
	this->rootDelay = rootDelay;
}

/// returns the root dispersion
ShortTimestamp NTPPacket::getRootdispersion() const
{
	return this->rootDispersion;
}

/// sets the root dispersion
void NTPPacket::setRootdispersion(ShortTimestamp rootDispersion)
{
	this->rootDispersion = rootDispersion;
}

/// returns the stratum number
/**
 * 0 means unspecified or invalid
 * 1 means primary server (directly connected to a time source)
 * 2-15 are secondary servers
 * 16 means unsychronized
 * 17-255 are reserved and therefore invalid!
 *
 * @return	stratum
 */
unsigned char NTPPacket::getStratum() const
{
	return this->stratum;
}

/// sets the stratum number
/**
 * see getStratum() for details which numbers are allowed.
 * Strata higher than 16 are considered false and will default to 0 (invalid).
 *
 * @param[in]	stratum
 */
void NTPPacket::setStratum(unsigned char stratum)
{
	if (stratum > 16)
	{
		stratum = 0; //invalid
	}
	this->stratum = stratum;
}

/// returns the transmit timestamp
LongTimestamp NTPPacket::getTransmitTime() const
{
	return this->transmitTimestamp;
}

/// sets the transmit timestamp
void NTPPacket::setTransmitTime(LongTimestamp transmitTime)
{
	this->transmitTimestamp = transmitTime;
}

} // end of namespace ntp
