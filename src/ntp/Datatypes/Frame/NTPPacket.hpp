/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file NTPPacket.hpp
 *
 * @date	25.03.2016
 * @author	silvan
 * @version	0.1
 */

#ifndef NTP_DATATYPES_FRAME_NTPPACKET_HPP_
#define NTP_DATATYPES_FRAME_NTPPACKET_HPP_

#include "../../Log/Logger.hpp"
#include "../Timestamp/LongTimestamp.hpp"
#include "../Timestamp/ShortTimestamp.hpp"
#include "ExtensionField.hpp"
#include <list>
#include <string>
#include <vector>

namespace ntp
{

/// minimal size of an NTP frame (without extension fields, digest and MAC) in bytes
#define MIN_FRAME_SIZE (12 * 4)

/**
 * @brief	leap indicator constants
 */
enum LeapIndicator : unsigned char
{
	/// no leap warning
	LI_NO_WARNING = 0,

	/// last minute of the day has 61 seconds
	LI_61SEC = 1,

	/// last minute of the day has 59 seconds
	LI_59SEC = 2,

	/// unknown (clock unsynchronized)
	LI_UNKNOWN = 3
};

/**
 * @brief	NTP Mode constants
 */
enum NTPMode
{
	/// reserved
	NTP_MODE_RESERVED = 0,

	/// symmetric active
	NTP_MODE_SYMACTIVE = 1,

	/// symmetric passive
	NTP_MODE_SYMPASSIVE = 2,

	/// client
	NTP_MODE_CLIENT = 3,

	/// server
	NTP_MODE_SERVER = 4,

	/// broadcast
	NTP_MODE_BROADCAST = 5,

	/// NTP control message
	NTP_MODE_CTRLMSG = 6,

	/// reserved for private use
	NTP_MODE_PRIVATE = 7
};

/// same as LI_UNKNOWN
#define LI_UNSYNCHRONIZED LI_UNKNOWN

/**
 * @brief 	This class represents a single NTP-Packet.
 * @details ntp packet class
 * @date 	21.02.2016
 * @author 	silvan
 * @version	0.1
 */
class NTPPacket
{
private:
	unsigned char leapIndicator;
	unsigned char version;
	unsigned char mode;
	unsigned char stratum;
	signed char poll;
	signed char precision;
	ShortTimestamp rootDelay;
	ShortTimestamp rootDispersion;
	char referenceID[5];
	LongTimestamp referenceTimestamp;
	LongTimestamp originTimestamp;
	LongTimestamp receiveTimestamp;
	LongTimestamp transmitTimestamp;
	std::list<ExtensionField> extensionFields;

	std::string refIDtoString() const;

public:
	NTPPacket(void);
	~NTPPacket(void);
	std::string toString() const;
	std::vector<unsigned char> createFrame();
	void addExtensionField(ExtensionField field);
	void loadFromUDP(std::vector<unsigned char> & data);

	//##########################################################################
	//################## set/get methods #######################################
	//##########################################################################

	std::list<ExtensionField>& getExtensionFields();

	void setLeapIndicator(LeapIndicator l);
	LeapIndicator getLeapIndicator() const;

	void setVersion(unsigned int version);
	unsigned int getVersion() const;

	void setMode(NTPMode m);
	NTPMode getMode() const;

	LongTimestamp getOriginTime() const;
	void setOriginTime(LongTimestamp originTime);

	signed char getPoll() const;
	void setPoll(signed char poll);

	signed char getPrecision() const;
	void setPrecision(signed char precision);

	LongTimestamp getReceiveTime() const;
	void setReceiveTime(LongTimestamp receiveTime);

	std::string getReferenceId() const;
	void setReferenceId(char const* reference_id);

	LongTimestamp getReferenceTime() const;
	void setReferenceTime(LongTimestamp referenceTime);

	ShortTimestamp getRootdelay() const;
	void setRootdelay(ShortTimestamp rootdelay);

	ShortTimestamp getRootdispersion() const;
	void setRootdispersion(ShortTimestamp rootdispersion);

	unsigned char getStratum() const;
	void setStratum(unsigned char stratum);

	LongTimestamp getTransmitTime() const;
	void setTransmitTime(LongTimestamp transmitTime);

}; // end class NTP_Packet

} // end of namespace ntp

#endif /* NTP_DATATYPES_FRAME_NTPPACKET_HPP_ */
