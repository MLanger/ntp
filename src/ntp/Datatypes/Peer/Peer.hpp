/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	Peer.hpp
 * @details This Header declares the Peer Class.
 * @date 	22.06.2016
 * @author 	Christian Jütte
 * @version	1.1
 */

#ifndef NTP_DATATYPES_PEER_PEER_HPP_
#define NTP_DATATYPES_PEER_PEER_HPP_

#include "../../Log/LogFile.hpp"
#include "../../Utility/ConfigFile.hpp"
#include "../../Utility/target_specific_mutex.hpp"
#include "../Frame/Message.hpp"
#include "../Frame/NTPPacket.hpp"
#include "../Timestamp/ShortTimestamp.hpp"
#include "PacketStatistics.hpp"
#include <algorithm> //std::sort
#include <array>
#include <bitset> // Output formatting
#include <cmath>
#include <iomanip> //Output formatting
#include <queue>

namespace ntp
{

/**
 * @brief 		This defines the Peer class for use in client-mode.
 * @details  	Peers are used when the application
 * 				runs in client-mode to keep track of the servers
 * 				the client is talking to.
 *
 * 				A lot of this class (mainly triedReach() and pollNow()) is also used
 * 				to implement the Poll Process as specified in RFC 5905, including Burst mechanisms.
 */

class Peer
{

private:
	//Member variables
	///For logging the packets received by this peer.
	LogFile packetStatLog;
	///Flag to tell whether logging the packets is enabled. Is set by config on construction.
	bool packetStatLogEnabled = true;
	///For logging the statistics of the peer coming out of the filter algorithm.
	LogFile peerStatLog;
	///Flag to tell whether logging the stats of the Peers is enabled. Is set by config on construction.
	bool peerStatLogEnabled = true;

	///Mutex for Thread-Safety
	std::mutex mtx;

	///IPv4-Address and Port of the Peer
	IPAddress address;

	///If the Peer was loaded with a hostname, this will be it.
	std::string hostname = "";

	///rootDelay of the latest packet
	ntp::ShortTimestamp rootDelay;

	///rootDispersion of the latest packet
	ntp::ShortTimestamp rootDispersion;

	///Reference ID of the latest packet
	std::string referenceID;

	///Reference Timestamp of the latest packet
	LongTimestamp referenceTimestamp;

	///Queue of PacketStatistics from received Packets
	std::deque<ntp::PacketStatistics> statisticsQueue;

	///Precision of the Peer
	unsigned char precision;

	///@name Calculated values for the peer
	///@{

	///Offset of the Peer in seconds
	double offset;

	///Delay of the Peer in seconds
	double delay;

	///Dispersion of the Peer
	double dispersion;

	///Jitter of the Peer
	double jitter;

	///Stratum of the Peer
	unsigned char stratum;

	///Last Leap-Indicator-Value
	ntp::LeapIndicator leapIndicator;

	///@}

	///@name Poll-Process Variables
	///@{
	///When enabled allows to send bursts on start and if a server has gone unreachable. Set on construction from config.
	bool initBurstEnabled;
	///When enabled allows to send bursts on each poll-interval. This is useful for large poll-intervals. Set on construction from config.
	bool burstEnabled;

	bool wasPolled; ///<First this is false until the Peer gets polled the first time.

	///Time of the latest, best sample
	std::chrono::steady_clock::time_point lastSynced;

	///Used to calculate when the next poll should happen, log2 seconds
	int hostPollExponent;

	///Reach-Register
	unsigned char reach;

	///Number of times the server was not reached (after the reach-register turned 0). Serves to adjust the hostPollExponent and thus the poll intervall.
	unsigned char unreach;

	///Time when the server was last polled
	std::chrono::steady_clock::time_point lastPoll;

	///This specifies the number of packets left to send if a burst is in progress. If no burst is in progress, this is 0.
	unsigned int burst;

	///@}

	/**
	 *
	 * @brief 	Used for the filter-Algorithm to order PacketStatistics
	 *
	 * @details Implements a function to be used with std::sort to sort PacketStatistics.
	 *
	 * @param [in] stat1 A pointer to an object of type PacketStatistics.
	 * @param [in] stat2 A pointer to an object of type PacketStatistics.
	 *
	 * @return	Returns true, if the delay of stat1 is smaller than the delay of stat2. Otherwise false.
	 *
	 */
	static bool compareStats(const ntp::PacketStatistics&, const ntp::PacketStatistics&);

public:
	Peer(IPAddress address);
	/// The copy-constructor is explicitly deleted since we don't want to copy the Peer. Somenone could use old data.
	Peer(const Peer&) = delete;
	~Peer();

	void processMessage(ntp::Message& message); //To process the stats of an
	                                            //incoming message by this Peer
	bool filter(bool synchronized);
	double getSyncDistance(); //Used by Select/Cluster/Combine-Algorithms
	bool isReachable();
	bool isAcceptable();
	void triedReach(); //Called upon a transmit to the Peer by the Synchronizer

	double getJitter();
	double getOffset();
	double getDelay();
	double getDispersion();
	ntp::ShortTimestamp getRootDelay();
	ntp::ShortTimestamp getRootDispersion();
	unsigned char getStratum();
	ntp::LeapIndicator getLeapIndicator();

	std::chrono::steady_clock::time_point getLastReceived();

	bool pollNow();
	IPAddress getAddress();
	unsigned char getPrecision();
	void setHostname(std::string hostname);
	std::string getHostname();

	void reset();
};

} // End of namespace ntp

#endif /* NTP_DATATYPES_PEER_PEER_HPP_ */
