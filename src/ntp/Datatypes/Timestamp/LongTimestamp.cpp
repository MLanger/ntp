/**
 * @file 	LongTimestamp.cpp
 * @details LongTimestamp type definition (64bit type).
 * @date 	24.03.2016
 * @author 	silvan
 * @version	0.1
 */

#include "LongTimestamp.hpp"
#include "../../Utility/KernelInterface.hpp"
#include <cmath>
#include <cstdint>
#include <iostream>

namespace ntp
{

/**
 * @brief	Simple constructor. Initializes timestamp with zero.
 *
 */
LongTimestamp::LongTimestamp(void) : Timestamp()
{
}

/**
 * @brief see Timestamp()
 */
LongTimestamp::LongTimestamp(double time) : Timestamp(time)
{
}

/**
 * @brief	Uses seconds and fraction to initialize the timestamp.
 *
 */
LongTimestamp::LongTimestamp(unsigned int seconds, unsigned int fraction) : Timestamp(seconds, fraction)
{
}

/**
 * @brief Returns a vector of bytes suitable for adding to a UDP frame.
 */
std::vector<unsigned char> LongTimestamp::getRaw() const
{
	uint32_t buffer[2];
	std::vector<unsigned char> data(sizeof(buffer));

	buffer[0] = KernelInterface::hostToNetworkOrder(static_cast<uint32_t>(this->seconds));
	buffer[1] = KernelInterface::hostToNetworkOrder(static_cast<uint32_t>(this->fraction));

	uint8_t* array = reinterpret_cast<unsigned char*>(buffer);
	for (unsigned int i = 0; i < sizeof(buffer); i++)
	{
		data[i] = array[i];
	}

	return data;
}

/**
 * @brief	creates a timestamp from raw data of type std::vector<unsigned char>
 * @details	This method takes the start position in a vector of unsigned char.
 * 			It will increment the iterator by 8. If start can not be incremented by eight,
 * 			this method may throw an exception when trying to dereference start, depending
 * 			on the implementation of std::vector.
 */
void LongTimestamp::loadFromRaw(vector_uchar_iter& start)
{
	// load seconds
	uint32_t seconds = 0;
	seconds = static_cast<uint32_t>(*start); // byte 1
	seconds <<= 8;
	start++;
	seconds += static_cast<uint32_t>(*start); // byte 2
	seconds <<= 8;
	start++;
	seconds += static_cast<uint32_t>(*start); // byte 3
	seconds <<= 8;
	start++;
	seconds += static_cast<uint32_t>(*start); // byte 4

	start++;

	this->seconds = static_cast<unsigned int>((seconds));

	// load fraction
	uint32_t fraction;

	fraction = static_cast<uint32_t>(*start); // byte 5
	fraction <<= 8;
	start++;
	fraction += static_cast<uint32_t>(*start); // byte 6
	fraction <<= 8;
	start++;
	fraction += static_cast<uint32_t>(*start); // byte 7
	fraction <<= 8;
	start++;
	fraction += static_cast<uint32_t>(*start); // byte 8
	start++;

	this->fraction = static_cast<unsigned int>(fraction);
}

/**
 * @brief	Subtracts two timestamps and returns the result as double.
 *
 * @details	This function has to be used since the actual subtraction should be done
 * 			in 64-Bit arithmetic, as recommended by RFC5905.
 * 			Only later is the value converted to double.
 *
 */

double LongTimestamp::operator-(LongTimestamp subtrahend)
{
	uint64_t thisTimestamp = seconds;
	thisTimestamp = thisTimestamp << 32;
	thisTimestamp += fraction;

	uint64_t subtr = subtrahend.getSeconds();
	subtr = subtr << 32;
	subtr += subtrahend.getFraction();

	double result = ((int64_t)(thisTimestamp - subtr)) * NTPLONG_FRAC_RESOLUTION;

	return result;
}

} // end of namespace ntp
