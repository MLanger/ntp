/**
 * @file 	Timestamp.cpp
 * @details Timestamp base type definition
 * @date 	05.06.2016
 * @author 	silvan, christian
 * @version	0.3
 */

#include "Timestamp.hpp"
#include <cmath>
#include <iostream>
#include <sstream>

namespace ntp
{

/**
 * @brief	Simple constructor. Initializes timestamp with zero.
 *
 */
Timestamp::Timestamp(void)
{
	this->seconds = 0;
	this->fraction = 0;
}

/**
 * @brief	Useful for casting between timestamps.
 */
Timestamp::Timestamp(double time)
{
	if (time < 0)
	{
		WARN("A timestamp was created from a negative time:" + std::to_string(time));
	}
	this->seconds = static_cast<uint32_t>(trunc(time));
	this->fraction = static_cast<uint32_t>((time - this->seconds) * UINT32_MAX);
}

/**
 * @brief	Virtual destructor. Necessary, because of virtual method.
 */
Timestamp::~Timestamp()
{
}

/**
 * @brief	Uses seconds and fraction to initialize the timestamp.
 */
Timestamp::Timestamp(unsigned int seconds, unsigned int fraction)
{
	this->seconds = seconds;
	this->fraction = fraction;
}

/**
 * @brief	Double interpretation of a timestamp.
 */
Timestamp::operator double() const
{
	double timestamp = static_cast<double>(this->seconds);
	timestamp += NTPLONG_FRAC_RESOLUTION * static_cast<double>(this->fraction);

	return timestamp;
}

/**
 * @brief Returns a vector of bytes suitable for adding to a UDP frame.
 */
std::vector<unsigned char> Timestamp::getRaw() const
{
	uint32_t buffer[2];
	std::vector<unsigned char> data(sizeof(buffer));
	buffer[0] = static_cast<uint32_t>(this->seconds);
	buffer[1] = static_cast<uint32_t>(this->fraction);

	uint8_t* array = reinterpret_cast<uint8_t*>(buffer);

	for (unsigned int i = 0; i < sizeof(buffer); i++)
	{
		data[i] = array[i];
	}

	return data;
}

/**
 * @brief	Returns a string representing the date and time of the timestamp.
 * @details	The string will be in the format: YYYY-M-D H:M:S
 *
 * @bug	This function currently converts the time to unix-time first and then creates a string out of it.
 * 			The problem with that is that there was no unix-time before 1970, however NTP-Timestamps start
 * 			in 1900.
 *
 * 			So currently, if the timestamp contains a value representing time before 1970, it is interpreted
 * 			as timestamp from the next NTP-Era (starting in 2036). This might lead to confusion when displaying
 * 			zero-timestamps.
 *
 * 			To resolve this, one should only use the NTP-Time and calculate the date from that, which is a little tricky
 * 			since leap years and everything need to be considered.
 *
 * @note	Currently, a value of 0 will always be displayed as 1900-01-01 00:00:00.
 */
std::string Timestamp::toString(void) const
{
	std::string output;
	if (this->seconds == 0)
	{
		output = "1900-01-01 00:00:00";
	}
	else
	{
		uint32_t unixtime = this->seconds;
		if (unixtime >= Timestamp::UNIX_NTP_TIME_OFFSET)
		{
			unixtime -= Timestamp::UNIX_NTP_TIME_OFFSET;
		}
		else
		{
			unixtime = unixtime + (UINT32_MAX - Timestamp::UNIX_NTP_TIME_OFFSET);
		}

		time_t xtime = unixtime;
		DEBUG(std::to_string(xtime));
		tm date;
		tm* dateptr = nullptr;

		dateptr = gmtime(&xtime);
		//		xtime = time(0);
		//		dateptr = gmtime( xtime );
		if (dateptr == nullptr)
		{
			//			date = 0;
			DEBUG("Date was initialized with nullptr!");
		}
		else
		{
			date = *dateptr;
			DEBUG("Date was translated correctly");
		}

		//gmtime_r(&time, &date);	// returns pointer to static data. pointer not used, because not threadsafe
		std::ostringstream converter;
		converter << date.tm_year + TM_START_YEAR << "-" << date.tm_mon + 1 << /* month starts at 0*/
		    "-" << date.tm_mday << " " << date.tm_hour << ":" << date.tm_min << ":" << date.tm_sec;
		output = converter.str();
	}

	return output;
}

/**
 * @brief	Returns the number of seconds since the NTP-Epoch. Note: This is different than the UTC-epoch.
 *
 */
uint32_t Timestamp::getSeconds() const
{
	return seconds;
}

/**
 * @brief	Returns the fraction of a second of the timestamp.
 *
 */
uint32_t Timestamp::getFraction() const
{
	return fraction;
}

} // end of namespace ntp
