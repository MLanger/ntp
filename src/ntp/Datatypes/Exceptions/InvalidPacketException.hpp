/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	InvalidPacketException.hpp
 * @details This declares the InvalidPacketException-Type
 * @date 	18.04.2016
 * @author 	silvan
 * @version	0.1
 */

#ifndef NTP_DATATYPES_EXCEPTIONS_INVALIDPACKETEXCEPTION_HPP_
#define NTP_DATATYPES_EXCEPTIONS_INVALIDPACKETEXCEPTION_HPP_

#include <exception>
#include <string>

namespace ntp
{

class InvalidPacketException : public std::exception
{
public:
	enum Reason
	{
		PACKET_TOO_SHORT,
		EXTENSION_FIELD_TOO_SHORT,
		EXTENSION_FIELD_WRONG_LENGTH
	};

	InvalidPacketException(std::string error, Reason r) noexcept;
	const char* what() const noexcept;
	std::string toString() const noexcept;
	bool hasReason(Reason r) const noexcept;

private:
	std::string error;
	Reason localReason;
};

} // end of namespace ntp

#endif /* NTP_DATATYPES_EXCEPTIONS_INVALIDPACKETEXCEPTION_HPP_ */
