/**
 * @file 	TimeoutException.cpp
 * @date 	29.10.2016
 * @author 	Christian Juette
 * @version	1.0
 *
 */

#include "TimeoutException.hpp"

namespace ntp
{

/**
 * @brief	Constructor of the TimeoutException.
 *
 * @param	[in]	error	A string containing a description of the error.
 *
 */

TimeoutException::TimeoutException(std::string error) noexcept
{
	this->error = error;
}

/**
 *
 * @brief	Returns a description of the Error that has happened.
 *
 * @returns	String describing the error.
 *
 */

std::string TimeoutException::toString() const noexcept
{
	return ("TimeoutException: " + error);
}

} // end of namespace ntp
