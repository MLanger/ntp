/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	NetworkException.hpp
 * @details This declares the NetworkException-Type
 * @date 	05.06.2016
 * @author 	Christian Juette
 * @version	1.0
 */

#ifndef NTP_DATATYPES_EXCEPTIONS_NETWORKEXCEPTION_HPP_
#define NTP_DATATYPES_EXCEPTIONS_NETWORKEXCEPTION_HPP_

#include <exception>
#include <string>

namespace ntp
{

class NetworkException : public std::exception
{
private:
	std::string error;
	bool fatal; ///Signals if the error is a fatal error or not
public:
	NetworkException(std::string error, bool fatal) noexcept;
	std::string toString() const noexcept;
	bool isFatal() const noexcept;
	const char* what() const throw() { return error.c_str(); }
};

} // end of namespace ntp

#endif /* NTP_DATATYPES_EXCEPTIONS_NETWORKEXCEPTION_HPP_*/
