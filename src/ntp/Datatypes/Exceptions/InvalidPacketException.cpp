/**
 * @file 	InvalidPacketException.cpp
 * @date 	18.04.2016
 * @author 	silvan
 * @version	0.1
 */

#include "InvalidPacketException.hpp"

namespace ntp
{

InvalidPacketException::InvalidPacketException(std::string error, Reason r) noexcept
{
	this->error = error;
	this->localReason = r;
}

const char* InvalidPacketException::what() const noexcept
{
	return "InvalidPacketException";
}

std::string InvalidPacketException::toString() const noexcept
{
	return std::string(what()) + ": " + this->error;
}

bool InvalidPacketException::hasReason(Reason r) const noexcept
{
	return this->localReason == r;
}

} // end of namespace ntp
