/**
 * @file SharedSystemVariables.cpp
 * @see SharedSystemVariables.hpp
 *
 * @date 30.08.2016
 * @author Christian Jütte
 * @version 0.1
 *
 * @warning ---
 * @copyright Copyright (c) 2016, Christian Jütte
 * 			  All rights reserved.
 *
 * @brief
 */

#include "SharedSystemVariables.hpp"

namespace ntp
{

/**
 * @brief	Standard-Constructor of the object. Initializes all fields to default values.
 *
 */
SharedSystemVariables::SharedSystemVariables()
{
	stratum = Constants::getInstance().getUChar(ntp::ConstantsUChar::maxStrat);
	poll = Constants::getInstance().getUChar(ntp::ConstantsUChar::pollMin);
	rootDelay = ShortTimestamp(0);
	rootDispersion = ShortTimestamp(Constants::getInstance().getDouble(ntp::ConstantsDouble::maxDisp));
	referenceTimestamp = LongTimestamp(0);
	leapIndicator = ntp::LeapIndicator::LI_UNKNOWN;
	for (int i = 0; i < 4; i++)
	{
		referenceID[i] = 0;
	}
}

/**
 * @brief	Destructor. Does nothing.
 *
 */
SharedSystemVariables::~SharedSystemVariables()
{
}

void SharedSystemVariables::setStratum(unsigned char stratum)
{
	this->stratum = stratum;
}

unsigned char SharedSystemVariables::getStratum()
{
	return stratum;
}

void SharedSystemVariables::setLeapIndicator(ntp::LeapIndicator leapIndicator)
{
	this->leapIndicator = leapIndicator;
}

ntp::LeapIndicator SharedSystemVariables::getLeapIndicator()
{
	return leapIndicator;
}

void SharedSystemVariables::setPoll(signed char poll)
{
	this->poll = poll;
}

signed char SharedSystemVariables::getPoll()
{
	return poll;
}

/**
 * @brief Adds step to the poll-Variable, and returns the new value.
 *
 */
signed char SharedSystemVariables::increasePoll(signed char step)
{
	poll += step;
	return poll;
}

/**
 * @brief Subtracts step from the poll-Variable, and returns the new value.
 *
 */
signed char SharedSystemVariables::decreasePoll(signed char step)
{
	poll -= step;
	return poll;
}

void SharedSystemVariables::setRootDelay(ShortTimestamp rootDelay)
{
	this->rootDelay = rootDelay;
}

ShortTimestamp SharedSystemVariables::getRootDelay()
{
	return rootDelay;
}

void SharedSystemVariables::setRootDispersion(ShortTimestamp rootDispersion)
{
	this->rootDispersion = rootDispersion;
}

ShortTimestamp SharedSystemVariables::getRootDispersion()
{
	return rootDispersion;
}

void SharedSystemVariables::setReferenceTimestamp(LongTimestamp referenceTimestamp)
{
	this->referenceTimestamp = referenceTimestamp;
}

LongTimestamp SharedSystemVariables::getReferenceTimestamp()
{
	return referenceTimestamp;
}

std::string SharedSystemVariables::getReferenceID() const
{
	return std::string(referenceID);
}

void SharedSystemVariables::setReferenceID(char const* referenceID)
{
	for (unsigned int i = 0; i < 4; i++)
	{
		this->referenceID[i] = referenceID[i];
	}
}

/**
 * @brief	load the variables from raw data, that should've been gotten from another process.
 * 			The ordering of the data is the same as described in the getRawData()-Function
 *
 * @see		getRawData
 */

void SharedSystemVariables::loadFromRaw(std::vector<unsigned char>& data)
{
	if (data.size() >= SIZE)
	{
		std::vector<unsigned char>::iterator cursor = data.begin();
		rootDelay.loadFromRaw(cursor);
		rootDispersion.loadFromRaw(cursor);
		referenceTimestamp.loadFromRaw(cursor);
		stratum = *cursor;
		cursor++;
		switch (*cursor)
		{
		case 0: leapIndicator = ntp::LeapIndicator::LI_NO_WARNING; break;
		case 1: leapIndicator = ntp::LeapIndicator::LI_61SEC; break;
		case 2: leapIndicator = ntp::LeapIndicator::LI_59SEC; break;
		case 3:
		default: leapIndicator = ntp::LeapIndicator::LI_UNKNOWN; break;
		}
		cursor++;
		poll = reinterpret_cast<signed char&>(*cursor);
		cursor++;
		for (int i = 0; i < 4; i++)
		{
			referenceID[i] = *(cursor + i);
		}
	}
	else
	{
		LOG_ERROR("Could not load shared system variables - too small!");
		//throw(FileException("Could not load shared system variables - too small!"));
	}
}

/**
 * @brief	Packs the variables in raw bytes that can be transferred to the other process.
 *
 * @details	The current format is this:
 *
 *	Bytes		|	Variable
 *	-----------	|	------------------
 *	0-3			|	Root Delay
 *	4-7			|	Root Dispersion
 *	8-15		|	Reference Timestamp
 *	16			|	Stratum
 *	17			|	LeapIndicator
 *	18			|	Poll
 *	19-22		|	Reference ID
 *
 */

std::vector<unsigned char> SharedSystemVariables::getRawData()
{
	std::vector<unsigned char> output;

	//Add root delay
	std::vector<unsigned char> temp = rootDelay.getRaw();
	output.insert(output.end(), temp.begin(), temp.end());

	//Add root dispersion
	temp = rootDispersion.getRaw();
	output.insert(output.end(), temp.begin(), temp.end());

	//Add reference timestamp
	temp = referenceTimestamp.getRaw();
	output.insert(output.end(), temp.begin(), temp.end());

	//Add stratum
	output.push_back(stratum);

	//Add LeapIndicator
	output.push_back(leapIndicator);

	//Add poll
	output.push_back(reinterpret_cast<unsigned char&>(poll));

	//Add reference ID
	for (int i = 0; i < 4; i++)
	{
		output.push_back(referenceID[i]);
	}

	return output;
}

/**
 * @brief	Returns the size in bytes a sharedSystemVariables object should use up in raw form
 *
 *
 */
unsigned char SharedSystemVariables::getRawSize()
{
	return SIZE;
}

/**
 * @brief	Creates a string with the contents for debugging purposes.
 *
 */
std::string SharedSystemVariables::toString()
{
	std::string output = "";
	output.append("Stratum: " + std::to_string(stratum) + ";");
	output.append("LeapIndicator: " + std::to_string(leapIndicator) + ";");
	output.append("Poll: " + std::to_string(poll) + ";");
	output.append("RootDelay: " + rootDelay.toString() + ";");
	output.append("RootDispersion: " + rootDispersion.toString() + ";");
	output.append("ReferenceTimestamp: " + referenceTimestamp.toString() + ";");
	if (stratum <= 1)
	{
		output.append("ReferenceID: " + std::string(referenceID) + ";");
	}
	else
	{
		output.append("ReferenceID: " + std::to_string((unsigned char)referenceID[0]) + "." + std::to_string((unsigned char)referenceID[1]) + "." + std::to_string((unsigned char)referenceID[2]) + "." + std::to_string((unsigned char)referenceID[3]) + ";");
	}

	return output;
}

} //end of namespace ntp
