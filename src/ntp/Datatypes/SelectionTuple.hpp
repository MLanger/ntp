/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file SelectionTuple.hpp
 * @brief Tuple to select the best communicating Peer.
 * @details It contains either the lower edge, the upper edge or the middle of an offset
 * 			inteval. This is specified with the type value.
 *
 * @date 2016
 * @author Simon Häußler
 * @version 0.13
 * @copyright Copyright 2016, Simon Häußler.
 * 			  All rights reserved.
 */

#ifndef NTP_DATATYPES_SELECTIONTUPLE_HPP_
#define NTP_DATATYPES_SELECTIONTUPLE_HPP_

#include "../Log/Logger.hpp"
#include "Peer/Peer.hpp"
#include <iostream>

namespace ntp
{

/**
 * @brief To define one offset interval tuple of a peer.
 * @details This class contains one edge of the offset-interval of one Peer.
 * 			It is used to select the best Time out of an array of Tuples.
 * 			The offset of one Peer decides in an interval, which is defined by two edges.
 * 			Each object of this class represents the lower edge (type = -1), the upper edge (type = +1)
 * 			or the middle (type = 0).
 * @date 28.03.2016
 * @author Simon Häußler
 * @version 0.1
 */
class SelectionTuple
{

private:
	//private attributes
	/**
	 * @brief To the tuple dedicated Peer.
	 */
	ntp::Peer* peer;

	/**
	 * @brief The type of this edge.
	 * @details The type is specified for the edge:<ul>
	 * 			<li> -1 => lower edge</li>
	 * 			<li> 0 	=> middle</li>
	 * 			<li> +1 => upper edge</li>
	 * 			</ul>
	 */
	int type;

	/**
	 * @brief Value of the edge tuple in seconds.
	 * @details It represents one of the following cases:<ul>
	 * 			<li>the lower edge of the offset interval -> edge = offset-lambda</li>
	 * 			<li>the upper edge of the offset interval -> edge = offset+lambda</li>
	 * 			<li>the middle of the offset interval	  -> edge = offset</li>
	 * 			</ul>
	 * 			Lambda is here the maximum error of the system clock of the time server.
	 */
	double edge;

public:
	//public functions
	SelectionTuple(ntp::Peer* peer, int type);
	~SelectionTuple(void);
	bool operator<(const SelectionTuple tuple) const;
	int getType();
	double getEdge();
	ntp::Peer* getPeer();

}; //end of class SelectionTuple

} //end of namespace ntp

#endif /* NTP_DATATYPES_SELECTIONTUPLE_HPP_ */
