/**
 * @file 	Logger.cpp
 * @details Simple Logging API
 * @date 	08.07.2016
 * @author 	silvan
 * @version	0.2
 */

#include "Logger.hpp"
#include "../Utility/KernelInterface.hpp"
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>

namespace ntp
{

std::chrono::steady_clock::time_point Logger::startTimeLog;

/**
 * @brief	Creates a Logger-instance, which uses the given loglevel and
 * 			chattyFlags.
 * @details	This class is implemented to become used as a singleton.
 * 			You should set loglevel and chattyFlags according to the
 * 			symbolic constants defined in Logger.hpp. The output is written to
 * 			filename and stdout.
 *
 * @param[in]	filename	path to the logging file
 * @param[in]	loglevel	the loglevel used to filter the output
 * @param[in]	chattyFlags	the flags which enable certain debugging output
 *
 * @warning	currently no logfile will be created
 */
Logger::Logger(std::string filename, int loglevel, unsigned char chattyFlags)
{
	this->loglevel = loglevel;
	this->chattyFlags = chattyFlags;
	this->enableConsoleOut = true; // for compatibility with old versions
	this->enableFileOut = false;   // for compatibility with old versions
	this->filename = filename;
	DEBUG("Logger initialized.");
}

/// returns a reference to the only instance of Logger
Logger& Logger::getInstance()
{
	return instance;
}

/**
 * @brief	logs an informal message
 * @details	Informal messages are messages, that inform the user
 * 			about operations going on in the system in general.
 * @param[in]	message		Message which will become printed int the logging-output
 */
void Logger::info(const std::string& message)
{
	if (this->loglevel >= LOG_INFO_LVL)
		output(message);
}

/**
 * @brief	logs a debug message useful for programmers
 * @details	Debug messages are intended to be informations for programmers.
 * 			They should be used to detect bugs and therefore should produce
 * 			a more detailed output than info-log-entries.
 * @param[in]	message		Message which will become printed int the logging-output
 * @see		info()
 */
void Logger::debug(const std::string& message)
{
	if (this->loglevel >= LOG_DEBUG_LVL)
		output(message);
}

/**
 * @brief	logs warnings useful for users to debug their configuration
 * @details	Warnings are intended to be information for users about
 * 			operations or configurations which may mislead to undesired
 * 			behaviour of the program.
 * @param[in]	message		Message which will become printed int the logging-output
 */
void Logger::warn(const std::string& message)
{
	if (this->loglevel >= LOG_WARN_LVL)
		output(message);
}

/**
 * @brief	logs errors
 * @details	Errors are program malfunctions which were caught during
 * 			the execution of the program. They may cause abort of an operation
 * 			or lead to the termination of the program. Errors are the most
 * 			important logging facility and should always become logged!
 * @param[in]	message		Message which will become printed int the logging-output
 */
void Logger::error(const std::string& message)
{
	if (this->loglevel >= LOG_ERROR_LVL)
		output(message);
}

/**
 * @brief	for debug information, which will be displayed frequently
 * @details	This one is similar to the debug()-method except, that it allows
 * 			filtering internally. It is possible to discard messages given to
 * 			this method. This behavior is especially useful for debug information
 * 			which will be given frequently but should not always become displayed
 * 			when the debug-level is set to LOG_DEBUG_LVL.
 *
 * 			Have a look at the constructor to discover how to disable/enable chatty
 * 			messages.
 * @param[in]	message		Message which will become printed int the logging-output
 */
void Logger::chattyPollProcess(const std::string& message)
{
	if ((this->loglevel >= LOG_DEBUG_LVL) && (this->chattyFlags & LOG_CHATTY_POLLPROCESS))
		output(message);
}

/**
 * @brief	for debug information, which will be displayed frequently
 * @details	This one is similar to the debug()-method except, that it allows
 * 			filtering internally. It is possible to discard messages given to
 * 			this method. This behavior is especially useful for debug information
 * 			which will be given frequently but should not always become displayed
 * 			when the debug-level is set to LOG_DEBUG_LVL.
 *
 * 			Have a look at the constructor to discover how to disable/enable chatty
 * 			messages.
 * @param[in]	message		Message which will become printed int the logging-output
 */
void Logger::chattyUpdateSystemvariables(const std::string& message)
{
	if ((this->loglevel >= LOG_DEBUG_LVL) && (this->chattyFlags & LOG_CHATTY_UPDATESYSTEMVARIABLES))
		output(message);
}

std::string Logger::padTo(const std::string& msg, unsigned int length)
{
	std::stringstream ss;
	ss << std::setw(length) << std::left << msg;
	return ss.str();
}

std::string Logger::padLocation(const char* filename, const char* function, unsigned int line, unsigned int length)
{
	std::stringstream ss1;
	std::stringstream ss2;

	ss1 << filename << ":" << line << " ";
	ss2 << std::setw(length) << std::left << ss1.str();

	return ss2.str();
}

/**
 * @brief	general output method for all logging facilities
 * @details	This method does directly output the message to
 * 			the outputstreams (e.g. file, stdout).
 * @param[in]	message		Message which will become printed in the logging-output
 */
void Logger::output(const std::string& message)
{
	std::lock_guard<std::mutex> lck(m_mutex);
	double time = this->getProcessTime();

	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);
	std::stringstream localTime;
	localTime << std::put_time(&tm, "[%d.%m.%Y  %H:%M:%S]  ");

	if (enableConsoleOut)
	{
		std::cout << localTime.str() << std::setw(13) << std::left << time << message << "\n";
	}

	if (enableFileOut)
	{
		std::ostringstream tmp;
		tmp << localTime.str() << std::setw(13) << std::left << time << message;
		logfile.appendLine(tmp.str());
	}
}

/**
 *
 * @brief	Function to get the current process-Time for purposes.
 *
 * @returns	The process time in seconds.
 *
 */
double Logger::getProcessTime()
{
	if (startTimeLog.time_since_epoch().count() == 0)
	{
		startTimeLog = std::chrono::steady_clock::now();
	}
	auto time = std::chrono::steady_clock::now();
	auto time_s = std::chrono::time_point_cast<std::chrono::seconds>(time);
	double ret = std::chrono::duration_cast<std::chrono::duration<double>>(time_s - startTimeLog).count();
	return ret;
}

/**
 * @brief	sets the Loglevel
 * @details	Sets loglevel as defined in Logger.hpp.
 */
void Logger::setLogLevel(int level)
{
	this->loglevel = level;
}

/**
 * @brief	enable console output
 * @details	If set to true, logging messages will be directed to the console.
 * 			If set to false, no messages will be directed to the console.
 */
void Logger::enableConsoleOutput(bool enable)
{
	this->enableConsoleOut = enable;
}

/**
 * @brief	enables output to file
 * @details	If set to true, logging messages will be directed to the logging file.
 * 			If set to false, no messages will be directed to the logging file.
 */
void Logger::enableFileOutput(bool enable)
{
	this->enableFileOut = enable;

	if (enable)
	{
		logfile.open(filename, true);
	}
	else if (logfile.isOpen())
	{
		logfile.close();
	}
}

/**
 * @brief	takes a string and interprets it as a loglevel
 * @details	Similar to the constants defined in Logger.hpp there are the
 * 			following valid strings: ERROR, INFO, WARN, DEBUG.
 * 			However, the string loglevel will be compared case insensitive
 * 			to those valid strings.
 *
 * 			Raises an invalid_argument exception if unable to decode
 * 			the given string.
 */
int Logger::logLevelFromString(std::string loglevel)
{
	std::map<int, std::string> logLevels;

	logLevels[10] = "ERROR";
	logLevels[20] = "INFO";
	logLevels[30] = "WARN";
	logLevels[50] = "DEBUG";

	std::string loglevelUpperCase;
	loglevelUpperCase.resize(loglevel.size());
	std::transform(loglevel.begin(), loglevel.end(), loglevelUpperCase.begin(), ::toupper);

	for (auto level : logLevels)
	{
		if (loglevelUpperCase == level.second)
		{
			return level.first;
		}
	}

	// no loglevel found:
	std::cerr << "Error: could not decode loglevel: " << loglevelUpperCase << std::endl;
	throw(std::invalid_argument("Unable to decode loglevel"));

	return 0;
}

/**
 * @brief	sets the new place, where the logging file should be stored
 * @details	The filename of the log file is defined in the macro LOG_FILE.
 * 			This method takes the filename and the directory and creates a complete
 * 			path which will be used as log-file.
 */
void Logger::setLogDir(std::string directory)
{
	std::string filePath = directory + "/" + this->filename;
	this->filename = filePath;

	if (logfile.isOpen())
	{
		logfile.close();
		// is only executed if the file was open previously
		if (!logfile.open(filePath, false))
		{
			//std::cerr << "Error opening logfile: " << filePath << std::endl;
		}
	}
}

Logger::~Logger()
{
	if (logfile.isOpen())
	{
		logfile.close();
	}
}

} // end of namespace ntp
